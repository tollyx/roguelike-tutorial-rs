# tollyx does the roguelike tutorial (in rust)

![Game Screenshot](screenshot.png)

[It's this tutorial right here that I'm following](https://bfnightly.bracketproductions.com/rustbook/chapter_0.html)

I've started so many roguelikes and never finished them because I've gotten stuck on either overengineering or just me doing bad choices somewhere, leading to me losing steam and putting the project on hold.

So this is me just following along and seeing how others do stuff just so I can start getting things done.
