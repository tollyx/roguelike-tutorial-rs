mod component;
mod debug;
mod gamelog;
mod gamestate;
mod gui;
mod map;
mod map_builders;
mod player;
mod random_table;
mod rect;
mod rex_assets;
mod saveload;
mod spawner;
mod systems;

use bracket_terminal::{embedded_resource, link_resource, prelude::*};

pub const WINDOW_WIDTH: i32 = 80;
pub const WINDOW_HEIGHT: i32 = 50;
pub const VERSION: &str = env!("CARGO_PKG_VERSION");
pub const SHOW_MAPGEN_VISUALIZER: bool = false;

embedded_resource!(TILES, "../resources/16x16.png");

fn main() {
    link_resource!(TILES, "../resources/16x16.png");
    let context = BTermBuilder::new()
        .with_title(format!("Legendary Spork - v{}", VERSION))
        // .with_tile_dimensions(12, 12)
        .with_tile_dimensions(16, 16)
        // .with_font("12x12.png", 12, 12)
        .with_font("16x16.png", 16, 16)
        .with_dimensions(WINDOW_WIDTH, WINDOW_HEIGHT)
        //.with_simple_console(WINDOW_WIDTH, WINDOW_HEIGHT, "12x12.png")
        .with_simple_console(WINDOW_WIDTH, WINDOW_HEIGHT, "16x16.png")
        .build()
        .expect("Failed building window");
    let gs = State {
        gamestate: gamestate::GameStateManager::new(Box::new(gamestate::mainmenu::MainMenu::new())),
    };
    main_loop(context, gs).expect("Main loop error");
}

pub struct State {
    gamestate: gamestate::GameStateManager,
}

impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm) {
        self.gamestate.update(ctx);
        self.gamestate.draw(ctx);
    }
}
