pub const DEBUG_FEATURES_ENABLED: bool = false;

pub fn timer<T>(name: &str, mut f: impl FnMut() -> T) -> T {
    if DEBUG_FEATURES_ENABLED {
        let start = std::time::Instant::now();
        let val = f();
        let end = std::time::Instant::now();
        println!("{} took {}µs", name, (end - start).as_micros());
        val
    } else {
        f()
    }
}
