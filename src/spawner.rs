use crate::{component::*, random_table::RandomTable, rect::Rect};
use bracket_color::prelude::*;
use bracket_random::prelude::RandomNumberGenerator;
use bracket_terminal::prelude::to_cp437;
use specs::prelude::*;
use specs::saveload::*;

pub fn player(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437('@'),
            fg: RGB::named(CYAN),
            bg: RGB::named(BLACK),
            render_order: 1,
        })
        .with(Player {})
        .with(Viewshed::new(8))
        .with(Name {
            name: "Player".to_string(),
        })
        .with(CombatStats {
            hp_max: 30,
            hp: 30,
            defense: 2,
            power: 5,
        })
        .with(HungerClock {
            state: HungerState::WellFed,
            duration: 20,
        })
        // .with(Invulnerable {})
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

pub fn room_table(map_depth: i32) -> RandomTable {
    RandomTable::new_with_entries(&[
        ("Goblin", 10),
        ("Orc", 1 + map_depth),
        ("Health Potion", 7),
        ("Fireball Scroll", 2 + map_depth),
        ("Confusion Scroll", 2 + map_depth),
        ("Magic Missile Scroll", 4),
        ("Dagger", 3),
        ("Shield", 3),
        ("Longsword", map_depth - 1),
        ("Tower Shield", map_depth - 1),
        ("Rations", 10),
        ("Magic Mapping Scroll", 2),
        ("Bear Trap", 2),
    ])
}

pub fn populate_room(
    rng: &mut RandomNumberGenerator,
    ecs: &mut World,
    room: &Rect,
    map_depth: i32,
) {
    let points: Vec<(i32, i32)> = (room.top..room.bottom)
        .flat_map(|y| (room.left..room.right).map(move |x| (x, y)))
        .collect();
    spawn_region(rng, ecs, &points, map_depth)
}

pub fn spawn_region(
    rng: &mut RandomNumberGenerator,
    ecs: &mut World,
    area: &[(i32, i32)],
    map_depth: i32,
) {
    let spawn_count = (rng.range(0, 9) + map_depth - 4)
        .max(0)
        .min(area.len() as i32);
    let mut possible_spawns = Vec::from(area);
    if spawn_count == 0 {
        return;
    }
    let spawns: Vec<_> = {
        let mut rng = ecs.fetch_mut::<RandomNumberGenerator>();
        (0..spawn_count)
            .map(|_| possible_spawns.remove(rng.random_slice_index(&possible_spawns).unwrap()))
            .collect()
    };
    let table = room_table(map_depth);
    for (x, y) in spawns {
        spawn_by_name(ecs, table.roll(rng).unwrap(), x, y);
    }
}

fn spawn_by_name(ecs: &mut World, name: &str, x: i32, y: i32) -> Option<Entity> {
    let ent = match name {
        "Goblin" => goblin(ecs, x, y),
        "Orc" => orc(ecs, x, y),
        "Health Potion" => health_potion(ecs, x, y),
        "Fireball Scroll" => fireball_scroll(ecs, x, y),
        "Confusion Scroll" => confusion_scroll(ecs, x, y),
        "Magic Missile Scroll" => magic_missile_scroll(ecs, x, y),
        "Dagger" => dagger(ecs, x, y),
        "Shield" => shield(ecs, x, y),
        "Longsword" => longsword(ecs, x, y),
        "Tower Shield" => tower_shield(ecs, x, y),
        "Rations" => rations(ecs, x, y),
        "Magic Mapping Scroll" => magic_mapping_scroll(ecs, x, y),
        "Bear Trap" => bear_trap(ecs, x, y),
        _ => {
            println!("Tried to spawn unknown entity '{}'", name);
            return None;
        }
    };
    Some(ent)
}

fn goblin(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437('g'),
            fg: RGB::named(GREEN),
            bg: RGB::named(BLACK),
            render_order: 1,
        })
        .with(Viewshed::new(8))
        .with(Monster {})
        .with(BlocksTile {})
        .with(Name {
            name: "Goblin".to_string(),
        })
        .with(CombatStats {
            hp_max: 16,
            hp: 16,
            defense: 1,
            power: 4,
        })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn orc(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437('o'),
            fg: RGB::named(DARK_GREEN),
            bg: RGB::named(BLACK),
            render_order: 1,
        })
        .with(Viewshed::new(8))
        .with(Monster {})
        .with(BlocksTile {})
        .with(Name {
            name: "Orc".to_string(),
        })
        .with(CombatStats {
            hp_max: 16,
            hp: 16,
            defense: 1,
            power: 4,
        })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn health_potion(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437('¡'),
            fg: RGB::named(MAGENTA),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Health Potion".to_string(),
        })
        .with(Item {})
        .with(Consumable {})
        .with(ProvidesHealing { heal_amount: 8 })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn magic_missile_scroll(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437(')'),
            fg: RGB::named(CYAN),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Magic Missile Scroll".to_string(),
        })
        .with(Item {})
        .with(Consumable {})
        .with(Ranged { range: 6 })
        .with(InflictsDamage { damage: 8 })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn fireball_scroll(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437(')'),
            fg: RGB::named(ORANGE),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Fireball Scroll".to_string(),
        })
        .with(Item {})
        .with(Consumable {})
        .with(Ranged { range: 6 })
        .with(InflictsDamage { damage: 20 })
        .with(AreaOfEffect { radius: 3 })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn confusion_scroll(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437(')'),
            fg: RGB::named(PINK),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Confusion Scroll".to_string(),
        })
        .with(Item {})
        .with(Consumable {})
        .with(Ranged { range: 6 })
        .with(Confusion { turns: 4 })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn dagger(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437('/'),
            fg: RGB::named(CYAN),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Dagger".to_string(),
        })
        .with(Equippable {
            slot: EquipmentSlot::Melee,
        })
        .with(Item {})
        .with(MeleePowerBonus { power: 2 })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn shield(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437('('),
            fg: RGB::named(CYAN),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Shield".to_string(),
        })
        .with(Equippable {
            slot: EquipmentSlot::Shield,
        })
        .with(Item {})
        .with(DefenseBonus { defense: 1 })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn longsword(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437('/'),
            fg: RGB::named(YELLOW),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Longsword".to_string(),
        })
        .with(Item {})
        .with(Equippable {
            slot: EquipmentSlot::Melee,
        })
        .with(MeleePowerBonus { power: 4 })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn tower_shield(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437('('),
            fg: RGB::named(YELLOW),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Tower Shield".to_string(),
        })
        .with(Item {})
        .with(Equippable {
            slot: EquipmentSlot::Shield,
        })
        .with(DefenseBonus { defense: 3 })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn rations(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437('%'),
            fg: RGB::named(GREEN),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Rations".to_string(),
        })
        .with(Item)
        .with(ProvidesFood)
        .with(Consumable)
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn magic_mapping_scroll(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437(')'),
            fg: RGB::named(CYAN3),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Scroll of Magic Mapping".to_string(),
        })
        .with(Item {})
        .with(MagicMapper {})
        .with(Consumable {})
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

fn bear_trap(ecs: &mut World, x: i32, y: i32) -> Entity {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437('^'),
            fg: RGB::named(RED),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .with(Name {
            name: "Bear Trap".to_string(),
        })
        .with(Hidden {})
        .with(EntryTrigger {})
        .with(InflictsDamage { damage: 6 })
        .with(SingleActivation)
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}
