use bracket_algorithm_traits::prelude::*;
use bracket_geometry::prelude::Point;
use bracket_terminal::prelude::FontCharType;
use serde::{Deserialize, Serialize};
use specs::Entity;
use std::collections::HashSet;

#[derive(PartialEq, Copy, Clone, Debug, Serialize, Deserialize)]
pub enum TileType {
    Wall,
    Floor,
    DownStairs,
}

#[derive(Clone)]
struct SpatialEntry {
    pub ent: Entity,
    pub blocking: bool,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Map {
    tiles: Vec<TileType>,
    width: i32,
    height: i32,
    revealed: Vec<bool>,
    visible: Vec<bool>,
    blocked: Vec<bool>,
    pub depth: i32,
    bloodstains: HashSet<usize>,

    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    tile_content: Vec<Vec<SpatialEntry>>,
}

impl Map {
    pub fn new(width: i32, height: i32, tile: TileType) -> Self {
        let len = (width * height) as usize;
        Self {
            tiles: vec![tile; len],
            width,
            height,
            revealed: vec![false; len],
            visible: vec![false; len],
            blocked: vec![false; len],
            tile_content: vec![vec![]; len],
            depth: 0,
            bloodstains: HashSet::new(),
        }
    }

    pub fn index(&self, x: i32, y: i32) -> usize {
        (y * self.width + x) as usize
    }

    pub fn index_to_coord(&self, i: usize) -> (i32, i32) {
        (i as i32 % self.width, i as i32 / self.width)
    }

    pub fn set_tile(&mut self, x: i32, y: i32, val: TileType) {
        if self.is_inside_bounds(x, y) {
            let i = self.index(x, y);
            self.tiles[i] = val
        }
    }

    pub fn set_tile_by_index(&mut self, i: usize, val: TileType) {
        if i < self.tiles.len() {
            self.tiles[i] = val
        }
    }

    pub fn is_blocked(&self, x: i32, y: i32) -> bool {
        if self.is_inside_bounds(x, y) {
            let i = self.index(x, y);
            self.blocked[i] || self.tile_content[i].iter().any(|e| e.blocking)
        } else {
            true
        }
    }

    pub fn populate_blocked(&mut self) {
        for (i, tile) in self.tiles.iter().enumerate() {
            self.blocked[i] = *tile == TileType::Wall;
        }
    }

    pub fn get_tile(&self, x: i32, y: i32) -> TileType {
        if self.is_inside_bounds(x, y) {
            self.tiles[self.index(x, y)]
        } else {
            TileType::Wall
        }
    }

    pub fn magic_mapper_tick(&mut self) -> bool {
        let mut points = vec![];
        for i in 0..self.tiles.len() {
            if !self.revealed[i] {
                let (x, y) = self.index_to_coord(i);
                let neigh = [
                    (x, y + 1),
                    (x, y - 1),
                    (x + 1, y),
                    (x + 1, y + 1),
                    (x + 1, y - 1),
                    (x - 1, y),
                    (x - 1, y + 1),
                    (x - 1, y - 1),
                ];

                for (nx, ny) in &neigh {
                    if self.is_revealed(*nx, *ny) && self.get_tile(*nx, *ny) != TileType::Wall {
                        points.push((x, y));
                        break;
                    }
                }
            }
        }
        if dbg!(points.len()) > 0 {
            for (x, y) in points {
                self.mark_tile_as_revealed(x, y);
            }
            false
        } else {
            true
        }
    }

    pub fn set_bloodstain(&mut self, x: i32, y: i32, bloody: bool) -> bool {
        if bloody {
            self.bloodstains.insert(self.index(x, y))
        } else {
            self.bloodstains.remove(&self.index(x, y))
        }
    }

    pub fn is_bloody(&self, x: i32, y: i32) -> bool {
        self.bloodstains.contains(&self.index(x, y))
    }

    pub fn is_revealed(&self, x: i32, y: i32) -> bool {
        *self.revealed.get(self.index(x, y)).unwrap_or(&false)
    }

    pub fn is_visible(&self, x: i32, y: i32) -> bool {
        *self.visible.get(self.index(x, y)).unwrap_or(&false)
    }

    pub fn mark_tile_as_visible(&mut self, x: i32, y: i32) {
        if self.is_inside_bounds(x, y) {
            let i = self.index(x, y);
            self.revealed[i] = true;
            self.visible[i] = true;
        }
    }

    pub fn mark_tile_as_revealed(&mut self, x: i32, y: i32) {
        if self.is_inside_bounds(x, y) {
            let i = self.index(x, y);
            self.revealed[i] = true;
        }
    }

    pub fn reveal_everything(&mut self) {
        self.revealed.iter_mut().for_each(|x| *x = true);
    }

    pub fn clear_visible(&mut self) {
        for t in &mut self.visible {
            *t = false
        }
    }

    pub fn index_entity(&mut self, x: i32, y: i32, ent: Entity, blocking: bool) {
        if self.is_inside_bounds(x, y) {
            let i = self.index(x, y);
            self.tile_content[i].push(SpatialEntry { ent, blocking });
        }
    }

    pub fn remove_from_entity_index(&mut self, x: i32, y: i32, ent: Entity) {
        if self.is_inside_bounds(x, y) {
            let i = self.index(x, y);
            self.tile_content[i] = self.tile_content[i]
                .iter()
                .filter(|e| e.ent != ent)
                .cloned()
                .collect();
        }
    }

    pub fn clear_content_index(&mut self) {
        for content in &mut self.tile_content {
            content.clear();
        }
    }

    pub fn reinit_content_index(&mut self) {
        self.tile_content = vec![vec![]; (self.width * self.height) as usize];
    }

    pub fn get_tile_content(&self, x: i32, y: i32) -> Option<Vec<Entity>> {
        if self.is_inside_bounds(x, y) {
            let i = self.index(x, y);
            Some(self.tile_content[i].iter().map(|e| e.ent).collect())
        } else {
            None
        }
    }

    pub fn width(&self) -> i32 {
        self.width
    }
    pub fn height(&self) -> i32 {
        self.height
    }

    pub fn is_inside_bounds(&self, x: i32, y: i32) -> bool {
        x >= 0 && x < self.width && y >= 0 && y < self.height
    }

    fn is_exit_valid(&self, x: i32, y: i32) -> bool {
        !self.is_blocked(x, y)
    }

    pub fn get_neightbours(&self, x: i32, y: i32) -> Vec<Point> {
        [
            (x - 1, y - 1),
            (x, y - 1),
            (x + 1, y - 1),
            (x - 1, y),
            (x + 1, y),
            (x - 1, y + 1),
            (x, y + 1),
            (x + 1, y + 1),
        ]
        .iter()
        .filter(|(x, y)| self.is_inside_bounds(*x, *y))
        .map(|(x, y)| Point { x: *x, y: *y })
        .collect()
    }
}

impl BaseMap for Map {
    fn is_opaque(&self, idx: usize) -> bool {
        self.tiles.get(idx) == Some(&TileType::Wall)
    }
    fn get_available_exits(&self, idx: usize) -> SmallVec<[(usize, f32); 10]> {
        let mut exits = SmallVec::with_capacity(10);

        let (x, y) = self.index_to_coord(idx);
        let w = self.width as usize;

        if self.is_exit_valid(x - 1, y) {
            exits.push((idx - 1, 1.0));
        }
        if self.is_exit_valid(x + 1, y) {
            exits.push((idx + 1, 1.0));
        }
        if self.is_exit_valid(x, y - 1) {
            exits.push((idx - w, 1.0));
        }
        if self.is_exit_valid(x, y + 1) {
            exits.push((idx + w, 1.0));
        }

        if self.is_exit_valid(x - 1, y - 1) {
            exits.push(((idx - w) - 1, 1.45));
        }
        if self.is_exit_valid(x + 1, y - 1) {
            exits.push(((idx - w) + 1, 1.45));
        }
        if self.is_exit_valid(x - 1, y + 1) {
            exits.push(((idx + w) - 1, 1.45));
        }
        if self.is_exit_valid(x + 1, y + 1) {
            exits.push(((idx + w) + 1, 1.45));
        }

        exits
    }
    fn get_pathing_distance(&self, _idx1: usize, _idx2: usize) -> f32 {
        1.0
    }
}

impl Algorithm2D for Map {
    fn dimensions(&self) -> Point {
        Point::new(self.width, self.height)
    }
    fn point2d_to_index(&self, pt: Point) -> usize {
        self.index(pt.x, pt.y)
    }
    fn in_bounds(&self, pos: Point) -> bool {
        self.is_inside_bounds(pos.x, pos.y)
    }
    fn index_to_point2d(&self, idx: usize) -> Point {
        let (x, y) = self.index_to_coord(idx);
        Point::new(x, y)
    }
}

pub fn wall_glyph(map: &Map, x: i32, y: i32) -> FontCharType {
    if !map.is_inside_bounds(x, y) {
        return 35;
    }

    fn is_wall(map: &Map, x: i32, y: i32) -> bool {
        map.get_tile(x, y) == TileType::Wall && map.is_revealed(x, y)
    }

    match (
        is_wall(map, x, y - 1), // West
        is_wall(map, x, y + 1), // East
        is_wall(map, x - 1, y), // North
        is_wall(map, x + 1, y), // South
    ) {
        // West, East, North, South
        (false, false, false, false) => 9, // No surrounding walls
        (false, false, _, _) => 205,       // W/E walls
        (_, _, false, false) => 186,       // N/S walls
        // We have to be specific with the rest
        (false, true, false, true) => 201,
        (false, true, true, false) => 187,
        (false, true, true, true) => 203,
        (true, false, false, true) => 200,
        (true, false, true, false) => 188,
        (true, false, true, true) => 202,
        (true, true, false, true) => 204,
        (true, true, true, false) => 185,
        (true, true, true, true) => 206,
    }
}
