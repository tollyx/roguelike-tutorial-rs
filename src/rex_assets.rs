use bracket_terminal::{embedded_resource, embedding::EMBED, link_resource, rex::XpFile};

embedded_resource!(SPORK_LOGO, "../resources/spork.xp");

pub struct RexAssets {
    pub menu: XpFile,
}

impl RexAssets {
    pub fn load() -> RexAssets {
        link_resource!(SPORK_LOGO, "../resources/spork.xp");
        RexAssets {
            menu: XpFile::from_resource("../resources/spork.xp").unwrap(),
        }
    }
}
