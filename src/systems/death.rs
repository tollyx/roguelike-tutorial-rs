use crate::{
    component::{CombatStats, Name, Position},
    gamelog::Logger,
    map::Map,
};
use specs::prelude::*;

pub struct DeathSystem {}

impl<'a> System<'a> for DeathSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        WriteExpect<'a, Map>,
        Entities<'a>,
        WriteStorage<'a, CombatStats>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Name>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (mut map, entities, combat_stats, positions, names) = data;

        for (ent, pos, name) in (
            &entities,
            &combat_stats,
            (&positions).maybe(),
            (&names).maybe(),
        )
            .join()
            .filter_map(|(e, s, p, n)| if s.hp <= 0 { Some((e, p, n)) } else { None })
        {
            if let Some(name) = name {
                Logger::new().npc_name(&name.name).append("died!").log();
            }
            if let Some(pos) = pos {
                map.remove_from_entity_index(pos.x, pos.y, ent);
            }
            // TODO: drop carried items
            // TODO: corpses
            entities.delete(ent).expect("Unable to delete dead entity")
        }
    }
}
