use crate::{component::*, gamelog::Logger, gamestate::playstate::RunState};
use bracket_color::prelude::*;
use specs::prelude::*;

pub struct HungerSystem {}

impl<'a> System<'a> for HungerSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, HungerClock>,
        ReadExpect<'a, PlayerEnt>,
        ReadExpect<'a, RunState>,
        WriteStorage<'a, SufferDamage>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (entities, mut hunger_clock, player_entity, runstate, mut inflict_damage) = data;
        for (ent, mut clock) in (&entities, &mut hunger_clock).join() {
            let proceed = match *runstate {
                RunState::PlayerTurn => ent == player_entity.0,
                RunState::MonsterTurn => ent != player_entity.0,
                _ => false,
            };
            if proceed {
                clock.duration -= 1;
                if clock.duration <= 0 {
                    match clock.state {
                        HungerState::WellFed => {
                            clock.state = HungerState::Sated;
                            clock.duration = 200;
                            if ent == player_entity.0 {
                                Logger::new()
                                    .color(ORANGE)
                                    .append("You are no longer well fed.")
                                    .log();
                            }
                        }
                        HungerState::Sated => {
                            clock.state = HungerState::Hungry;
                            clock.duration = 200;
                            if ent == player_entity.0 {
                                Logger::new().color(ORANGE).append("You are hungry.").log();
                            }
                        }
                        HungerState::Hungry => {
                            clock.state = HungerState::Starving;
                            clock.duration = 200;
                            if ent == player_entity.0 {
                                Logger::new().color(RED).append("You are starving!").log();
                            }
                        }
                        HungerState::Starving => {
                            clock.duration = 4;
                            if ent == player_entity.0 {
                                Logger::new()
                                    .color(RED)
                                    .append("You suffer 1 damage from starvation!")
                                    .log();
                            }
                            SufferDamage::new_damage(&mut inflict_damage, ent, 1);
                        }
                    }
                }
            }
        }
    }
}
