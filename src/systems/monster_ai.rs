use super::ParticleBuilder;
use crate::{component::*, gamestate::playstate::RunState, map::Map};
use bracket_color::prelude::*;
use bracket_geometry::prelude::Point;
use bracket_pathfinding::prelude::{a_star_search, DistanceAlg};
use bracket_terminal::prelude::to_cp437;
use specs::prelude::*;

pub struct MonsterAiSystem {}

impl<'a> System<'a> for MonsterAiSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Entities<'a>,
        WriteExpect<'a, Map>,
        ReadExpect<'a, PlayerPos>,
        ReadExpect<'a, PlayerEnt>,
        WriteStorage<'a, Viewshed>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, WantsToMelee>,
        WriteStorage<'a, Confusion>,
        WriteStorage<'a, EntityMoved>,
        ReadStorage<'a, Monster>,
        ReadExpect<'a, RunState>,
        WriteExpect<'a, ParticleBuilder>,
    );

    fn run(
        &mut self,
        (
            ents,
            mut map,
            player_pos,
            player_ent,
            mut viewsheds,
            mut positions,
            mut melees,
            mut confusion,
            mut moved,
            monsters,
            runstate,
            mut particles,
        ): Self::SystemData,
    ) {
        if *runstate != RunState::MonsterTurn {
            return;
        }
        for (ent, mut viewshed, mut pos, _monster) in
            (&ents, &mut viewsheds, &mut positions, &monsters).join()
        {
            let can_act = if let Some(confused) = confusion.get_mut(ent) {
                confused.turns -= 1;
                if confused.turns <= 0 {
                    confusion.remove(ent);
                }

                particles.request(
                    pos.x,
                    pos.y,
                    RGB::named(MAGENTA),
                    RGB::named(BLACK),
                    to_cp437('?'),
                    200.0,
                );

                false
            } else {
                true
            };

            if !can_act {
                continue;
            }

            if viewshed.visible_tiles.contains(&player_pos.0) {
                let distance =
                    DistanceAlg::Pythagoras.distance2d(Point::new(pos.x, pos.y), player_pos.0);
                if distance < 1.5 {
                    melees
                        .insert(
                            ent,
                            WantsToMelee {
                                target: player_ent.0,
                            },
                        )
                        .expect("Unable to target player");
                } else {
                    let path = a_star_search(
                        map.index(pos.x, pos.y) as i32,
                        map.index(player_pos.0.x, player_pos.0.y) as i32,
                        &*map,
                    );
                    if path.success && path.steps.len() > 1 {
                        let (nx, ny) = map.index_to_coord(path.steps[1]);
                        map.remove_from_entity_index(pos.x, pos.y, ent);
                        pos.x = nx;
                        pos.y = ny;
                        viewshed.dirty = true;
                        map.index_entity(pos.x, pos.y, ent, true);
                        moved.insert(ent, EntityMoved).unwrap();
                    }
                }
            }
        }
    }
}
