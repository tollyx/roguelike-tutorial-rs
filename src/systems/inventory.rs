use super::ParticleBuilder;
use crate::{component::*, gamelog::Logger, gamestate::playstate::RunState, map::Map};
use bracket_color::prelude::*;
use bracket_geometry::prelude::Point;
use bracket_pathfinding::prelude::field_of_view;
use bracket_terminal::prelude::to_cp437;
use specs::prelude::*;

pub struct ItemPickupSystem {}

impl<'a> System<'a> for ItemPickupSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        ReadExpect<'a, Map>,
        Entities<'a>,
        WriteStorage<'a, WantsToPickupItem>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, Name>,
        WriteStorage<'a, InContainer>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (map, entities, mut wants_pickup, mut positions, names, mut backpack) = data;

        for (ent, pickup) in (&entities, &wants_pickup).join() {
            let pos = positions.remove(pickup.item);
            backpack
                .insert(pickup.item, InContainer { container: ent })
                .expect("Unable to insert backpack entry");

            if let (Some(inv_name), Some(item_name), Some(pos)) =
                (names.get(ent), names.get(pickup.item), pos)
            {
                if map.is_visible(pos.x, pos.y) {
                    Logger::new()
                        .npc_name(&inv_name.name)
                        .append("picks up")
                        .npc_name(&item_name.name)
                        .log();
                }
            }
        }

        wants_pickup.clear();
    }
}

pub struct ItemRemoveSystem {}

impl<'a> System<'a> for ItemRemoveSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        ReadExpect<'a, PlayerEnt>,
        Entities<'a>,
        WriteStorage<'a, WantsToRemoveItem>,
        WriteStorage<'a, Equipped>,
        WriteStorage<'a, InContainer>,
        ReadStorage<'a, Name>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (player_ent, entities, mut wants_remove, mut equipped, mut backpack, names) = data;

        for (entity, to_remove, name) in (&entities, &wants_remove, (&names).maybe()).join() {
            if entity == player_ent.0 {
                Logger::new()
                    .append("You unequip the ")
                    .item_name(name.map(|n| n.name.as_str()).unwrap_or("item"))
                    .log();
            }
            equipped.remove(to_remove.item);
            backpack
                .insert(to_remove.item, InContainer { container: entity })
                .expect("Unable to insert backpack");
        }

        wants_remove.clear();
    }
}

pub struct ItemDropSystem {}

impl<'a> System<'a> for ItemDropSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        ReadExpect<'a, PlayerEnt>,
        Entities<'a>,
        WriteStorage<'a, WantsToDropItem>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, Name>,
        WriteStorage<'a, InContainer>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (player_entity, entities, mut wants_drop, mut positions, names, mut backpack) = data;
        for (entity, to_drop) in (&entities, &wants_drop).join() {
            let mut dropper_pos: Position = Position { x: 0, y: 0 };
            {
                let dropped_pos = positions.get(entity).unwrap();
                dropper_pos.x = dropped_pos.x;
                dropper_pos.y = dropped_pos.y;
            }
            positions
                .insert(
                    to_drop.item,
                    Position {
                        x: dropper_pos.x,
                        y: dropper_pos.y,
                    },
                )
                .expect("Unable to insert position");
            backpack.remove(to_drop.item);

            if entity == player_entity.0 {
                Logger::new()
                    .append("You drop the ")
                    .item_name(&names.get(to_drop.item).unwrap().name)
                    .log();
            }
        }

        wants_drop.clear();
    }
}

pub struct ItemUseSystem {}

impl<'a> System<'a> for ItemUseSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        ReadExpect<'a, PlayerEnt>,
        ReadExpect<'a, Map>,
        WriteExpect<'a, RunState>,
        Entities<'a>,
        WriteStorage<'a, WantsToUseItem>,
        ReadStorage<'a, ProvidesHealing>,
        ReadStorage<'a, ProvidesFood>,
        WriteStorage<'a, HungerClock>,
        ReadStorage<'a, InflictsDamage>,
        ReadStorage<'a, AreaOfEffect>,
        WriteStorage<'a, Confusion>,
        WriteStorage<'a, SufferDamage>,
        ReadStorage<'a, Consumable>,
        ReadStorage<'a, Name>,
        WriteStorage<'a, CombatStats>,
        ReadStorage<'a, Equippable>,
        WriteStorage<'a, Equipped>,
        WriteStorage<'a, InContainer>,
        WriteExpect<'a, ParticleBuilder>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, MagicMapper>,
    );

    #[allow(clippy::cognitive_complexity)]
    fn run(&mut self, data: Self::SystemData) {
        let (
            player_ent,
            map,
            mut runstate,
            entities,
            mut wants_use,
            healing,
            food,
            mut hunger,
            inflict_damage,
            area_of_effect,
            mut confusion,
            mut suffer_damage,
            consumables,
            names,
            mut combat_stats,
            equippable,
            mut equipped,
            mut in_container,
            mut particles,
            positions,
            magic_mapper,
        ) = data;

        for (ent, usable) in (&entities, &wants_use).join() {
            let item_name = names.get(usable.item).map_or("item", |n| n.name.as_str());
            let targets = get_targeted_entities(
                ent,
                usable.item,
                &usable.target,
                &map,
                &area_of_effect,
                &mut particles,
            );

            if let Some(pot) = healing.get(usable.item) {
                for mob in &targets {
                    if let Some(stats) = combat_stats.get_mut(*mob) {
                        stats.hp = (stats.hp + pot.heal_amount).min(stats.hp_max);
                        if ent == player_ent.0 {
                            if player_ent.0 == *mob {
                                Logger::new()
                                    .append("You use the")
                                    .item_name(item_name)
                                    .append("and heal")
                                    .append_color(GREEN, pot.heal_amount)
                                    .append("hp.")
                                    .log();
                            } else {
                                let mob_name = names.get(*mob).unwrap();
                                Logger::new()
                                    .append("You use the")
                                    .item_name(item_name)
                                    .append("on")
                                    .npc_name(&mob_name.name)
                                    .append("healing")
                                    .append_color(GREEN, pot.heal_amount)
                                    .append("hp.")
                                    .log();
                            }
                        }
                        if let Some(pos) = positions.get(*mob) {
                            particles.request(
                                pos.x,
                                pos.y,
                                RGB::named(GREEN),
                                RGB::named(BLACK),
                                to_cp437('♥'),
                                200.0,
                            )
                        }
                    }
                }
            }

            if food.get(usable.item).is_some() {
                for mob in &targets {
                    if let Some(mut hc) = hunger.get_mut(*mob) {
                        hc.state = HungerState::WellFed;
                        hc.duration = 20;
                        if player_ent.0 == *mob {
                            Logger::new()
                                .append("You ate the")
                                .item_name(item_name)
                                .log();
                        }
                    }
                }
            }

            if let Some(dmg) = inflict_damage.get(usable.item) {
                for mob in &targets {
                    if combat_stats.get_mut(*mob).is_some() {
                        SufferDamage::new_damage(&mut suffer_damage, *mob, dmg.damage);
                        if ent == player_ent.0 {
                            if player_ent.0 == *mob {
                                Logger::new()
                                    .append("You use the")
                                    .item_name(item_name)
                                    .append("and take")
                                    .damage(dmg.damage)
                                    .append("damage.")
                                    .log();
                            } else {
                                let mob_name = names.get(*mob).unwrap();
                                Logger::new()
                                    .append("Your")
                                    .item_name(item_name)
                                    .append("hits")
                                    .npc_name(&mob_name.name)
                                    .append("inflicting")
                                    .damage(dmg.damage)
                                    .append("damage.")
                                    .log();
                            }
                        }
                    }
                }
            }

            if let Some(turns) = confusion.get(usable.item).map(|c| c.turns) {
                for mob in &targets {
                    confusion
                        .insert(*mob, Confusion { turns })
                        .expect("Cound't confuse mob");
                    if player_ent.0 == *mob {
                        Logger::new()
                            .append("You use the")
                            .item_name(item_name)
                            .append("and become confused.")
                            .log();
                    } else {
                        let mob_name = names.get(*mob).unwrap();
                        Logger::new()
                            .append("Your")
                            .item_name(item_name)
                            .append("hits")
                            .npc_name(&mob_name.name)
                            .append("confusing them.")
                            .log();
                    }
                    if let Some(pos) = positions.get(*mob) {
                        particles.request(
                            pos.x,
                            pos.y,
                            RGB::named(MAGENTA),
                            RGB::named(BLACK),
                            to_cp437('?'),
                            200.0,
                        )
                    }
                }
            }

            if let Some(can_equip) = equippable.get(usable.item) {
                if let Some(target) = targets.get(0) {
                    let to_unequip: Vec<_> = (&entities, &equipped, (&names).maybe())
                        .join()
                        .filter(|(_, eq, _)| eq.owner == *target && eq.slot == can_equip.slot)
                        .inspect(|(_, _, name)| {
                            if *target == player_ent.0 {
                                Logger::new()
                                    .append("You unequip the")
                                    .item_name(name.map(|n| n.name.as_str()).unwrap_or("item"))
                                    .log();
                            }
                        })
                        .map(|(e, ..)| e)
                        .collect();
                    for item in to_unequip {
                        equipped.remove(item);
                        in_container
                            .insert(item, InContainer { container: *target })
                            .expect("Unable to insert unequipped item into inventory");
                    }

                    equipped
                        .insert(
                            usable.item,
                            Equipped {
                                owner: *target,
                                slot: can_equip.slot,
                            },
                        )
                        .expect("Unable to insert item into equipped");
                    if *target == player_ent.0 {
                        Logger::new()
                            .append("You equip the")
                            .item_name(item_name)
                            .log();
                    }
                    in_container.remove(usable.item);
                }
            }

            if magic_mapper.get(usable.item).is_some() {
                *runstate = RunState::MagicMapper;
                Logger::new().append("The map is revealed to you!").log();
            }

            if consumables.get(usable.item).is_some() {
                entities
                    .delete(usable.item)
                    .expect("Couldn't delete consumable item");
            }
        }
        wants_use.clear();
    }
}

fn get_targeted_entities(
    user: Entity,
    item: Entity,
    target: &Option<Point>,
    map: &Map,
    aoe: &ReadStorage<'_, AreaOfEffect>,
    particles: &mut ParticleBuilder,
) -> Vec<Entity> {
    match target {
        None => vec![user],
        Some(point) => match aoe.get(item) {
            Some(aoe) => field_of_view(*point, aoe.radius, &*map)
                .iter()
                .inspect(|p| {
                    particles.request(
                        p.x,
                        p.y,
                        RGB::named(ORANGE),
                        RGB::named(BLACK),
                        to_cp437('░'),
                        200.0,
                    )
                })
                .filter_map(|t| map.get_tile_content(t.x, t.y))
                .flatten()
                .collect(),
            None => map
                .get_tile_content(point.x, point.y)
                .unwrap_or_else(Vec::new),
        },
    }
}
