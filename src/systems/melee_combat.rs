use super::ParticleBuilder;
use crate::{
    component::{
        CombatStats, DefenseBonus, Equipped, HungerClock, HungerState, MeleePowerBonus, Name,
        Position, SufferDamage, WantsToMelee,
    },
    gamelog::Logger,
};
use bracket_color::prelude::*;
use bracket_terminal::prelude::to_cp437;
use specs::prelude::*;

pub struct MeleeCombatSystem {}

impl<'a> System<'a> for MeleeCombatSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, WantsToMelee>,
        ReadStorage<'a, Name>,
        ReadStorage<'a, CombatStats>,
        WriteStorage<'a, SufferDamage>,
        ReadStorage<'a, DefenseBonus>,
        ReadStorage<'a, MeleePowerBonus>,
        ReadStorage<'a, Equipped>,
        WriteExpect<'a, ParticleBuilder>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, HungerClock>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            entities,
            mut wants_melee,
            names,
            combat_stats,
            mut inflict_damage,
            defense_bonus,
            melee_bonus,
            equipped,
            mut particles,
            positions,
            hunger,
        ) = data;
        for (entity, wants_melee, name, stats, hc) in (
            &entities,
            &wants_melee,
            (&names).maybe(),
            &combat_stats,
            (&hunger).maybe(),
        )
            .join()
        {
            if let Some(target_stats) = combat_stats.get(wants_melee.target) {
                let mut atk_buff: i32 = (&melee_bonus, &equipped)
                    .join()
                    .filter_map(|(mb, eq)| {
                        if eq.owner == entity {
                            Some(mb.power)
                        } else {
                            None
                        }
                    })
                    .sum();

                if let Some(hc) = hc {
                    if hc.state == HungerState::WellFed {
                        atk_buff += 1;
                    }
                }

                let def_buff: i32 = (&defense_bonus, &equipped)
                    .join()
                    .filter_map(|(db, eq)| {
                        if eq.owner == entity {
                            Some(db.defense)
                        } else {
                            None
                        }
                    })
                    .sum();

                let target_name = names
                    .get(wants_melee.target)
                    .map(|n| n.name.as_str())
                    .unwrap_or("something");
                let attacker_name = name.map(|n| n.name.as_str()).unwrap_or("Something");

                let damage = 0.max((stats.power + atk_buff) - (target_stats.defense + def_buff));

                if damage == 0 {
                    Logger::new()
                        .npc_name(attacker_name)
                        .append("is unable to hurt")
                        .npc_name(target_name)
                        .log();
                } else {
                    if let Some(pos) = positions.get(wants_melee.target) {
                        particles.request(
                            pos.x,
                            pos.y,
                            RGB::named(ORANGE),
                            RGB::named(BLACK),
                            to_cp437('‼'),
                            200.0,
                        );
                    }
                    Logger::new()
                        .npc_name(attacker_name)
                        .append("hits")
                        .npc_name(target_name)
                        .append("for")
                        .damage(damage)
                        .append("hp.")
                        .log();
                    SufferDamage::new_damage(&mut inflict_damage, wants_melee.target, damage);
                }
            }
        }

        wants_melee.clear();
    }
}
