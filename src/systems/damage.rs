use crate::{
    component::{CombatStats, Invulnerable, Position, SufferDamage},
    map::Map,
};
use specs::prelude::*;

pub struct DamageSystem {}

impl<'a> System<'a> for DamageSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        WriteStorage<'a, CombatStats>,
        WriteStorage<'a, SufferDamage>,
        ReadStorage<'a, Invulnerable>,
        ReadStorage<'a, Position>,
        WriteExpect<'a, Map>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (mut stats, mut damage, invulnerable, positions, mut map) = data;

        for (mut stats, damage, _invul, pos) in
            (&mut stats, &damage, !&invulnerable, (&positions).maybe()).join()
        {
            stats.hp -= damage.amount.iter().sum::<i32>();
            if let Some(pos) = pos {
                map.set_bloodstain(pos.x, pos.y, true);
            }
        }

        damage.clear();
    }
}
