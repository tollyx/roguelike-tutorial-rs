use super::ParticleBuilder;
use crate::{
    component::{
        EntityMoved, EntryTrigger, Hidden, InflictsDamage, Name, Position, SingleActivation,
        SufferDamage,
    },
    gamelog::Logger,
    map::Map,
};
use bracket_color::prelude::*;
use bracket_terminal::prelude::to_cp437;
use specs::prelude::*;

pub struct TriggerSystem {}

impl<'a> System<'a> for TriggerSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        ReadExpect<'a, Map>,
        WriteStorage<'a, EntityMoved>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, EntryTrigger>,
        ReadStorage<'a, InflictsDamage>,
        WriteStorage<'a, SufferDamage>,
        WriteStorage<'a, Hidden>,
        ReadStorage<'a, Name>,
        ReadStorage<'a, SingleActivation>,
        Entities<'a>,
        WriteExpect<'a, ParticleBuilder>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            map,
            mut entity_moved,
            position,
            entry_trigger,
            inflicts_damage,
            mut suffer_damage,
            mut hidden,
            names,
            single_activation,
            entities,
            mut particle_builder,
        ) = data;

        let mut to_be_removed = vec![];

        // Iterate the entities that moved and their final position
        for (entity, mut _entity_moved, pos) in (&entities, &mut entity_moved, &position).join() {
            for entity_id in map.get_tile_content(pos.x, pos.y).unwrap() {
                if entity != entity_id {
                    // Do not bother to check yourself for being a trap!
                    if entry_trigger.get(entity_id).is_some() {
                        // We triggered it
                        if let Some(name) = names.get(entity_id) {
                            Logger::new().npc_name(&name.name).append("triggers!");
                        }
                        if let Some(dmg) = inflicts_damage.get(entity_id) {
                            particle_builder.request(
                                pos.x,
                                pos.y,
                                RGB::named(ORANGE),
                                RGB::named(BLACK),
                                to_cp437('‼'),
                                200.0,
                            );
                            SufferDamage::new_damage(&mut suffer_damage, entity_id, dmg.damage);
                        }

                        if single_activation.get(entity_id).is_some() {
                            to_be_removed.push(entity_id);
                        }

                        hidden.remove(entity_id); // The trap is no longer hidden
                    }
                }
            }
        }

        to_be_removed
            .iter()
            .for_each(|e| entities.delete(*e).unwrap());

        // Remove all entity movement markers
        entity_moved.clear();
    }
}
