use crate::component::{ParticleLifetime, Position, Renderable};
use bracket_color::prelude::*;
use bracket_terminal::prelude::{BTerm, FontCharType};
use specs::prelude::*;

pub fn cull_dead_particles(ecs: &mut World, ctx: &BTerm) {
    let dead_particles: Vec<Entity> = {
        let entities = ecs.entities();
        let mut particles = ecs.write_storage::<ParticleLifetime>();
        (&entities, &mut particles)
            .join()
            .filter_map(|(e, p)| {
                p.lifetime_ms -= ctx.frame_time_ms;
                if p.lifetime_ms <= 0. {
                    Some(e)
                } else {
                    None
                }
            })
            .collect()
    };
    ecs.delete_entities(&dead_particles)
        .expect("couldn't delete dead particles");
}

struct ParticleRequest {
    x: i32,
    y: i32,
    fg: RGB,
    bg: RGB,
    glyph: FontCharType,
    lifetime_ms: f32,
}

pub struct ParticleBuilder {
    requests: Vec<ParticleRequest>,
}

impl ParticleBuilder {
    #[allow(clippy::new_without_default)]
    pub fn new() -> ParticleBuilder {
        ParticleBuilder { requests: vec![] }
    }

    pub fn request(
        &mut self,
        x: i32,
        y: i32,
        fg: RGB,
        bg: RGB,
        glyph: FontCharType,
        lifetime_ms: f32,
    ) {
        self.requests.push(ParticleRequest {
            x,
            y,
            fg,
            bg,
            glyph,
            lifetime_ms,
        });
    }
}

pub struct ParticleSpawnSystem {}

impl<'a> System<'a> for ParticleSpawnSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Renderable>,
        WriteStorage<'a, ParticleLifetime>,
        WriteExpect<'a, ParticleBuilder>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (entities, mut positions, mut renderables, mut particles, mut particle_builder) = data;
        for new_particle in &particle_builder.requests {
            let ent = entities.create();
            positions
                .insert(
                    ent,
                    Position {
                        x: new_particle.x,
                        y: new_particle.y,
                    },
                )
                .expect("Unable to inser position");
            renderables
                .insert(
                    ent,
                    Renderable {
                        fg: new_particle.fg,
                        bg: new_particle.bg,
                        glyph: new_particle.glyph,
                        render_order: 0,
                    },
                )
                .expect("Unable to insert renderable");
            particles
                .insert(
                    ent,
                    ParticleLifetime {
                        lifetime_ms: new_particle.lifetime_ms,
                    },
                )
                .expect("Unable to insert lifetime");
        }

        particle_builder.requests.clear();
    }
}
