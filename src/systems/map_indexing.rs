use crate::{
    component::{BlocksTile, Position},
    map::Map,
};
use specs::prelude::*;

pub struct MapIndexingSystem {}

impl<'a> System<'a> for MapIndexingSystem {
    type SystemData = (
        WriteExpect<'a, Map>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, BlocksTile>,
        Entities<'a>,
    );

    fn run(&mut self, (mut map, positions, blockers, entities): Self::SystemData) {
        map.clear_content_index();
        map.populate_blocked();
        for (ent, pos, blocker) in (&entities, &positions, (&blockers).maybe()).join() {
            map.index_entity(pos.x, pos.y, ent, blocker.is_some());
        }
    }
}
