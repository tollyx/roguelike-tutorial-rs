use crate::{
    component::{Hidden, Name, Player, Position, Viewshed},
    gamelog::Logger,
    map::Map,
};
use bracket_pathfinding::prelude::field_of_view;
use bracket_random::prelude::RandomNumberGenerator;
use specs::prelude::*;

pub struct VisibilitySystem {}

impl<'a> System<'a> for VisibilitySystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        WriteExpect<'a, Map>,
        WriteExpect<'a, RandomNumberGenerator>,
        WriteStorage<'a, Viewshed>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Hidden>,
        ReadStorage<'a, Player>,
        ReadStorage<'a, Name>,
    );

    fn run(
        &mut self,
        (mut map, mut rng, mut viewshed, pos, mut hidden, players, names): Self::SystemData,
    ) {
        for (viewshed, pos, player) in (&mut viewshed, &pos, (&players).maybe())
            .join()
            .filter(|(v, ..)| v.dirty)
        {
            viewshed.visible_tiles = field_of_view((pos.x, pos.y).into(), viewshed.range, &*map);
            viewshed.dirty = false;

            if player.is_some() {
                map.clear_visible();
                for vis in &viewshed.visible_tiles {
                    map.mark_tile_as_visible(vis.x, vis.y);
                    if let Some(ents) = map.get_tile_content(vis.x, vis.y) {
                        for ent in ents {
                            if hidden.get(ent).is_some() && rng.range(0, 24) == 0 {
                                hidden.remove(ent);
                                if let Some(name) = names.get(ent) {
                                    Logger::new()
                                        .append("You spotted a")
                                        .npc_name(&name.name)
                                        .log();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
