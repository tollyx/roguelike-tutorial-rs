use crate::component::*;
use crate::{gamelog::Logger, map::*};
use bracket_terminal::prelude::{BTerm, VirtualKeyCode};
use specs::prelude::*;
use std::cmp::{max, min};

pub enum Input {
    Move { x: i32, y: i32 },
    GetItem,
    OpenInventory,
    RemoveItem,
    DropItem,
    Wait,
    Quit,
    GoDown,
}

pub fn get_input(ctx: &BTerm) -> Option<Input> {
    match ctx.key {
        None => None,
        Some(key) => match key {
            // Orthogonal movement
            VirtualKeyCode::Left | VirtualKeyCode::H | VirtualKeyCode::Numpad4 => {
                Some(Input::Move { x: -1, y: 0 })
            }
            VirtualKeyCode::Right | VirtualKeyCode::L | VirtualKeyCode::Numpad6 => {
                Some(Input::Move { x: 1, y: 0 })
            }
            VirtualKeyCode::Up | VirtualKeyCode::K | VirtualKeyCode::Numpad8 => {
                Some(Input::Move { x: 0, y: -1 })
            }
            VirtualKeyCode::Down | VirtualKeyCode::J | VirtualKeyCode::Numpad2 => {
                Some(Input::Move { x: 0, y: 1 })
            }

            // Diagonal movement
            VirtualKeyCode::Y | VirtualKeyCode::Numpad7 => Some(Input::Move { x: -1, y: -1 }),
            VirtualKeyCode::U | VirtualKeyCode::Numpad9 => Some(Input::Move { x: 1, y: -1 }),
            VirtualKeyCode::B | VirtualKeyCode::Numpad1 => Some(Input::Move { x: -1, y: 1 }),
            VirtualKeyCode::N | VirtualKeyCode::Numpad3 => Some(Input::Move { x: 1, y: 1 }),

            // Actions
            VirtualKeyCode::G => Some(Input::GetItem),
            VirtualKeyCode::I => Some(Input::OpenInventory),
            VirtualKeyCode::R => Some(Input::RemoveItem),
            VirtualKeyCode::D => Some(Input::DropItem),
            VirtualKeyCode::RBracket | VirtualKeyCode::OEM102 | VirtualKeyCode::PageDown => {
                Some(Input::GoDown)
            }

            VirtualKeyCode::Period | VirtualKeyCode::Numpad5 => Some(Input::Wait),
            VirtualKeyCode::Escape => Some(Input::Quit),
            _ => None,
        },
    }
}

pub fn try_move(delta_x: i32, delta_y: i32, ecs: &mut World) -> bool {
    let mut positions = ecs.write_storage::<Position>();
    let mut players = ecs.write_storage::<Player>();
    let mut viewsheds = ecs.write_storage::<Viewshed>();
    let mut moved = ecs.write_storage::<EntityMoved>();
    let ents = ecs.entities();
    let stats = ecs.read_storage::<CombatStats>();
    let mut melee = ecs.write_storage::<WantsToMelee>();
    let mut player_pos = ecs.write_resource::<PlayerPos>();
    let map = ecs.fetch::<Map>();

    let mut success = false;
    for (ent, pos, _player, viewshed) in
        (&ents, &mut positions, &mut players, &mut viewsheds).join()
    {
        let dest_pos = (pos.x + delta_x, pos.y + delta_y);

        if let Some(targets) = map.get_tile_content(dest_pos.0, dest_pos.1) {
            for target in targets {
                if let Some(_stat) = stats.get(target) {
                    melee
                        .insert(ent, WantsToMelee { target })
                        .expect("Add target failed");
                    success = true;
                    continue;
                }
            }
        }

        if !map.is_blocked(dest_pos.0, dest_pos.1) {
            pos.x = min(map.width() - 1, max(0, dest_pos.0));
            pos.y = min(map.height() - 1, max(0, dest_pos.1));
            viewshed.dirty = true;
            player_pos.0.x = pos.x;
            player_pos.0.y = pos.y;
            moved.insert(ent, EntityMoved).unwrap();
            success = true;
        }
    }
    success
}

pub fn skip_turn(ecs: &mut World) {
    let player_ent = ecs.fetch::<PlayerEnt>().0;
    let viewsheds = ecs.read_storage::<Viewshed>();
    let monsters = ecs.read_storage::<Monster>();
    let hungers = ecs.read_storage::<HungerClock>();

    let map = ecs.fetch::<Map>();

    let player_viewshed = viewsheds.get(player_ent).unwrap();
    let can_heal = player_viewshed
        .visible_tiles
        .iter()
        .flat_map(|t| map.get_tile_content(t.x, t.y).unwrap())
        .all(|e| monsters.get(e).is_none())
        && hungers
            .get(player_ent)
            .unwrap()
            .state
            .allows_passive_healing();

    if can_heal {
        let mut stats = ecs.write_storage::<CombatStats>();
        let player_stats = stats.get_mut(player_ent).unwrap();
        player_stats.hp = (player_stats.hp + 1).min(player_stats.hp_max);
    }
}

pub fn try_next_level(ecs: &mut World) -> bool {
    let player_pos = ecs.fetch::<PlayerPos>().0;
    let map = ecs.fetch::<Map>();

    if map.get_tile(player_pos.x, player_pos.y) == TileType::DownStairs {
        true
    } else {
        Logger::new().append("You can't go down here!").log();
        false
    }
}

pub fn try_get_item(ecs: &mut World) -> bool {
    let player_pos = ecs.fetch::<PlayerPos>();
    let player_entity = ecs.fetch::<PlayerEnt>();
    let entities = ecs.entities();
    let items = ecs.read_storage::<Item>();
    let positions = ecs.read_storage::<Position>();

    let mut target_item: Option<Entity> = None;
    for (item_entity, _item, position) in (&entities, &items, &positions).join() {
        if position.x == player_pos.0.x && position.y == player_pos.0.y {
            target_item = Some(item_entity);
        }
    }

    match target_item {
        None => {
            Logger::new()
                .append("There is nothing here to pick up.")
                .log();
            false
        }
        Some(item) => {
            let mut pickup = ecs.write_storage::<WantsToPickupItem>();
            pickup
                .insert(player_entity.0, WantsToPickupItem { item })
                .expect("Unable to insert want to pickup");
            true
        }
    }
}
