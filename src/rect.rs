use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, Copy, PartialEq)]
pub struct Rect {
    pub left: i32,
    pub right: i32,
    pub top: i32,
    pub bottom: i32,
}

impl Rect {
    pub fn new(x: i32, y: i32, w: i32, h: i32) -> Rect {
        Rect {
            left: x,
            right: x + w,
            top: y,
            bottom: y + h,
        }
    }

    pub fn width(&self) -> i32 {
        self.right - self.left
    }

    pub fn height(&self) -> i32 {
        self.bottom - self.top
    }

    pub fn intersects(&self, other: &Rect) -> bool {
        self.left <= other.right
            && self.right >= other.left
            && self.top <= other.bottom
            && self.bottom >= other.top
    }

    pub fn center(&self) -> (i32, i32) {
        ((self.left + self.right) / 2, (self.top + self.bottom) / 2)
    }

    pub fn dimensions(&self) -> (i32, i32) {
        (self.width(), self.height())
    }
}

impl From<Rect> for bracket_geometry::prelude::Rect {
    fn from(r: Rect) -> Self {
        bracket_geometry::prelude::Rect::with_exact(r.left, r.top, r.right, r.bottom)
    }
}
