mod damage;
mod death;
mod hunger;
mod inventory;
mod map_indexing;
mod melee_combat;
mod monster_ai;
mod particles;
mod trigger;
mod visibility;

pub use damage::*;
pub use death::*;
pub use hunger::*;
pub use inventory::*;
pub use map_indexing::*;
pub use melee_combat::*;
pub use monster_ai::*;
pub use particles::*;
pub use trigger::*;
pub use visibility::*;

pub fn new_dispatcher<'a, 'b>() -> specs::Dispatcher<'a, 'b> {
    specs::DispatcherBuilder::new()
        // Pre-turn systems
        .with(MapIndexingSystem {}, "map_indexing", &[])
        .with(VisibilitySystem {}, "visibility", &[])
        .with(MonsterAiSystem {}, "monster_ai", &[])
        // Item systems
        .with(ItemPickupSystem {}, "item_pickup", &[])
        .with(ItemDropSystem {}, "item_drop", &[])
        .with(ItemRemoveSystem {}, "item_remove", &[])
        .with(ItemUseSystem {}, "item_use", &[])
        .with(HungerSystem {}, "hunger", &[])
        // Combat systems
        .with(MeleeCombatSystem {}, "melee_combat", &[])
        .with(TriggerSystem {}, "trigger", &[])
        .with(DamageSystem {}, "damage", &[])
        // Post-turn systems
        .with(DeathSystem {}, "death", &[])
        .with(ParticleSpawnSystem {}, "particle_spawn", &[])
        .build()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::component;
    use crate::map::*;
    use bracket_random::prelude::RandomNumberGenerator;
    use specs::prelude::*;

    #[test]
    pub fn map_collision_indexing_on_entity_removal() {
        // This test makes sure that a tile is not marked as blocked after an entity dies.

        let mut ecs = World::new();
        let mut systems = new_dispatcher();
        let (x, y) = (5, 5);
        component::register_all(&mut ecs);

        ecs.insert(Map::new(10, 10, TileType::Floor));
        ecs.insert(RandomNumberGenerator::new());

        // create dead blocking entity that will be deleted
        let ent = ecs
            .create_entity()
            .with(component::Position { x, y })
            .with(component::BlocksTile)
            .with(component::CombatStats {
                hp: 0,
                defense: 0,
                hp_max: 10,
                power: 0,
            })
            .build();

        ecs.insert(component::PlayerEnt(ent));
        ecs.insert(component::PlayerPos((x, y).into()));

        systems.dispatch(&ecs);
        ecs.maintain();

        let map = ecs.fetch::<Map>();
        // the tile which the entity was in should not be blocked
        assert!(!map.is_blocked(x, y));
    }
}
