use crate::{component::Position, map::*};
use bracket_random::prelude::RandomNumberGenerator;
use specs::World;

pub mod bsp_dungeon;
pub mod bsp_interior;
pub mod cellular_automata;
pub mod common;
pub mod simple;

pub trait MapBuilder {
    fn build_map(&mut self);
    fn spawn_entities(&mut self, ecs: &mut World);
    fn get_map(&mut self) -> Map;
    fn get_starting_position(&mut self) -> Position;
    fn get_snapshot_history(&self) -> &Vec<Map>;
}

pub fn random_builder(rng: &mut RandomNumberGenerator, depth: i32) -> Box<dyn MapBuilder> {
    match rng.range(0, 4) {
        0 => Box::new(simple::SimpleMapBuilder::new(rng.rand(), depth)),
        1 => Box::new(bsp_dungeon::BspDungeonBuilder::new(rng.rand(), depth)),
        2 => Box::new(bsp_interior::BspInteriorBuilder::new(rng.rand(), depth)),
        3 => Box::new(cellular_automata::CellularAutomataBuilder::new(
            rng.rand(),
            depth,
        )),
        _ => unreachable!(),
    }
}
