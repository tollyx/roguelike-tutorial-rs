use bracket_color::prelude::*;
use bracket_terminal::prelude::TextBuilder;
use lazy_static::lazy_static;
use std::sync::Mutex;

pub struct LogFragment {
    pub color: RGB,
    pub text: String,
}

lazy_static! {
    static ref LOG: Mutex<Vec<Vec<LogFragment>>> = Mutex::new(Vec::new());
}

pub fn append_entry(fragments: Vec<LogFragment>) {
    LOG.lock().unwrap().push(fragments);
}

pub fn clear_log() {
    LOG.lock().unwrap().clear();
}

pub fn log_display(lines: usize) -> TextBuilder {
    let mut buf = TextBuilder::empty();
    LOG.lock()
        .unwrap()
        .iter()
        .rev()
        .take(lines)
        .for_each(|log| {
            log.iter().for_each(|frag| {
                buf.fg(frag.color);
                buf.line_wrap(&frag.text);
            });
            buf.ln();
        });
    buf
}

pub struct Logger {
    current_color: RGB,
    fragments: Vec<LogFragment>,
}

impl Logger {
    pub fn new() -> Self {
        Logger {
            current_color: RGB::named(WHITE),
            fragments: vec![],
        }
    }

    pub fn color(mut self, color: (u8, u8, u8)) -> Self {
        self.current_color = RGB::named(color);
        self
    }

    pub fn append<T: ToString>(mut self, text: T) -> Self {
        self.fragments.push(LogFragment {
            color: self.current_color,
            text: text.to_string(),
        });
        self
    }

    pub fn append_color<T: ToString>(mut self, color: (u8, u8, u8), text: T) -> Self {
        self.fragments.push(LogFragment {
            color: RGB::named(color),
            text: text.to_string(),
        });
        self
    }

    pub fn npc_name<T: ToString>(self, text: T) -> Self {
        self.append_color(YELLOW, text)
    }

    pub fn item_name<T: ToString>(self, text: T) -> Self {
        self.append_color(CYAN, text)
    }

    pub fn damage(self, dmg: i32) -> Self {
        self.append_color(RED, dmg)
    }

    pub fn log(self) {
        append_entry(self.fragments)
    }
}
