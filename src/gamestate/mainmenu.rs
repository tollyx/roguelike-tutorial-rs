use super::{GameState, GameStateAction};
use crate::rex_assets::RexAssets;
use bracket_color::prelude::*;
use bracket_terminal::prelude::{BTerm, DrawBatch, VirtualKeyCode};

#[derive(Copy, Clone, PartialEq)]
pub enum MainMenuSelection {
    NewGame,
    LoadGame,
    Quit,
}

pub struct MainMenu {
    selection: MainMenuSelection,
    assets: RexAssets,
}

impl MainMenu {
    pub fn new() -> MainMenu {
        MainMenu {
            selection: if crate::saveload::save_exists() {
                MainMenuSelection::LoadGame
            } else {
                MainMenuSelection::NewGame
            },
            assets: crate::rex_assets::RexAssets::load(),
        }
    }
}

impl GameState for MainMenu {
    fn update(&mut self, term: &BTerm) -> Option<GameStateAction> {
        let mut submit = false;
        self.selection = match term.key {
            None => self.selection,
            Some(key) => match key {
                VirtualKeyCode::Escape => MainMenuSelection::Quit,
                VirtualKeyCode::Up | VirtualKeyCode::K | VirtualKeyCode::Numpad8 => {
                    match self.selection {
                        MainMenuSelection::NewGame => MainMenuSelection::Quit,
                        MainMenuSelection::LoadGame => MainMenuSelection::NewGame,
                        MainMenuSelection::Quit => {
                            if crate::saveload::save_exists() {
                                MainMenuSelection::LoadGame
                            } else {
                                MainMenuSelection::NewGame
                            }
                        }
                    }
                }
                VirtualKeyCode::Down | VirtualKeyCode::J | VirtualKeyCode::Numpad2 => {
                    match self.selection {
                        MainMenuSelection::NewGame => {
                            if crate::saveload::save_exists() {
                                MainMenuSelection::LoadGame
                            } else {
                                MainMenuSelection::Quit
                            }
                        }
                        MainMenuSelection::LoadGame => MainMenuSelection::Quit,
                        MainMenuSelection::Quit => MainMenuSelection::NewGame,
                    }
                }
                VirtualKeyCode::Return | VirtualKeyCode::NumpadEnter => {
                    submit = true;
                    self.selection
                }
                _ => self.selection,
            },
        };

        if submit {
            match self.selection {
                MainMenuSelection::NewGame => Some(GameStateAction::Push(Box::new(
                    super::playstate::PlayState::new_game(),
                ))),
                MainMenuSelection::LoadGame => Some(GameStateAction::Push(Box::new(
                    super::playstate::PlayState::load_game(),
                ))),
                MainMenuSelection::Quit => Some(GameStateAction::Pop),
            }
        } else {
            None
        }
    }
    fn draw(&self, term: &mut BTerm) {
        let mut draw = DrawBatch::new();

        let color_selected = ColorPair::new(RGB::named(MAGENTA), RGB::named(BLACK));
        let color_not_selected = ColorPair::new(RGB::named(WHITE), RGB::named(BLACK));
        let color_disabled = ColorPair::new(RGB::named(DARK_GREY), RGB::named(BLACK));

        term.render_xp_sprite(&self.assets.menu, 1, 17);

        let save_exists = crate::saveload::save_exists();
        draw.print_color(
            (2, 31).into(),
            "New Game",
            if self.selection == MainMenuSelection::NewGame {
                color_selected
            } else {
                color_not_selected
            },
        );

        draw.print_color(
            (2, 32).into(),
            "Load Game",
            if save_exists {
                if self.selection == MainMenuSelection::LoadGame {
                    color_selected
                } else {
                    color_not_selected
                }
            } else {
                color_disabled
            },
        );

        draw.print_color(
            (2, 33).into(),
            "Quit",
            if self.selection == MainMenuSelection::Quit {
                color_selected
            } else {
                color_not_selected
            },
        );

        draw.submit(0).unwrap();
    }
}
