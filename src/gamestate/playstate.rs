use super::{GameState, GameStateAction};
use crate::{
    component::*, gamelog::Logger, gui, map::Map, map_builders, player, saveload, spawner, systems,
    SHOW_MAPGEN_VISUALIZER,
};
use bracket_color::prelude::*;
use bracket_random::prelude::RandomNumberGenerator;
use bracket_terminal::prelude::DrawBatch;
use player::Input;
use specs::prelude::*;

#[derive(PartialEq, Copy, Clone)]
pub enum RunState {
    AwaitingInput,
    PreRun,
    PlayerTurn,
    MonsterTurn,
    ShowInventory,
    ShowUnequipMenu,
    ShowDropMenu,
    ShowTargeting { range: i32, item: Entity },
    MagicMapper,
    NextLevel,
    MapGen,
    SaveGame,
    GameOver,
}

pub struct PlayState {
    dispatcher: Dispatcher<'static, 'static>,
    ecs: World,
    mapgen_history: Vec<Map>,
    mapgen_index: usize,
    mapgen_timer: f32,
}

impl GameState for PlayState {
    fn update(&mut self, term: &bracket_terminal::prelude::BTerm) -> Option<GameStateAction> {
        let mut runstate = *self.ecs.fetch::<RunState>();

        if runstate == RunState::MapGen {
            self.mapgen_timer += term.frame_time_ms;
            if self.mapgen_timer > 300.0 {
                self.mapgen_timer = 0.0;
                self.mapgen_index += 1;
                if self.mapgen_history.len() <= self.mapgen_index {
                    *self.ecs.write_resource() = RunState::PreRun;
                }
            }
            return None;
        }

        match runstate {
            RunState::PreRun => {
                self.dispatcher.dispatch(&self.ecs);
                self.ecs.maintain();
                runstate = RunState::AwaitingInput;
            }
            RunState::MapGen => {}
            RunState::AwaitingInput => {
                if !self.ecs.is_alive(self.ecs.fetch::<PlayerEnt>().0) {
                    Logger::new().color(RED).append("Game over.").log();
                    saveload::delete_save();
                    runstate = RunState::GameOver;
                } else if let Some(input) = player::get_input(&term) {
                    match input {
                        Input::Move { x, y } => {
                            if player::try_move(x, y, &mut self.ecs) {
                                runstate = RunState::PlayerTurn;
                            }
                        }
                        Input::GetItem => {
                            if player::try_get_item(&mut self.ecs) {
                                runstate = RunState::PlayerTurn;
                            }
                        }
                        Input::DropItem => {
                            runstate = RunState::ShowDropMenu;
                        }
                        Input::OpenInventory => {
                            runstate = RunState::ShowInventory;
                        }
                        Input::RemoveItem => {
                            runstate = RunState::ShowUnequipMenu;
                        }
                        Input::Wait => {
                            player::skip_turn(&mut self.ecs);
                            runstate = RunState::PlayerTurn;
                        }
                        Input::Quit => runstate = RunState::SaveGame,
                        Input::GoDown => {
                            if player::try_next_level(&mut self.ecs) {
                                runstate = RunState::NextLevel;
                            }
                        }
                    }
                }
            }
            RunState::PlayerTurn => {
                self.dispatcher.dispatch(&self.ecs);
                self.ecs.maintain();
                if *self.ecs.fetch::<RunState>() == RunState::MagicMapper {
                    runstate = RunState::MagicMapper;
                } else {
                    runstate = RunState::MonsterTurn;
                }
            }
            RunState::MonsterTurn => {
                self.dispatcher.dispatch(&self.ecs);
                self.ecs.maintain();
                runstate = RunState::AwaitingInput;
            }
            RunState::NextLevel => {
                self.goto_next_level();
                if SHOW_MAPGEN_VISUALIZER {
                    runstate = RunState::MapGen;
                } else {
                    runstate = RunState::PreRun;
                }
            }
            RunState::SaveGame => {
                saveload::save_game_to_file(&mut self.ecs);
                self.ecs.delete_all();
                return Some(GameStateAction::Pop);
            }
            RunState::GameOver => {
                if let Some(Input::Quit) = player::get_input(&term) {
                    return Some(GameStateAction::Pop);
                }
            }
            RunState::MagicMapper => {
                let mut map = self.ecs.fetch_mut::<Map>();
                let done = map.magic_mapper_tick();
                if done {
                    runstate = RunState::MonsterTurn;
                }
            }
            RunState::ShowInventory => match gui::inventory_input(&self.ecs, term) {
                (gui::ItemMenuResult::Cancel, _) => {
                    runstate = RunState::AwaitingInput;
                }
                (gui::ItemMenuResult::Selected, Some(item)) => {
                    if let Some(ranged) = self.ecs.read_storage::<Ranged>().get(item) {
                        runstate = RunState::ShowTargeting {
                            range: ranged.range,
                            item,
                        };
                    } else {
                        self.ecs
                            .write_storage()
                            .insert(
                                self.ecs.fetch::<PlayerEnt>().0,
                                WantsToUseItem { item, target: None },
                            )
                            .expect("Failed to use item");
                        runstate = RunState::PlayerTurn;
                    }
                }
                _ => {}
            },
            RunState::ShowDropMenu => match gui::inventory_input(&self.ecs, term) {
                (gui::ItemMenuResult::Cancel, _) => {
                    runstate = RunState::AwaitingInput;
                }
                (gui::ItemMenuResult::Selected, Some(item)) => {
                    self.ecs
                        .write_storage()
                        .insert(self.ecs.fetch::<PlayerEnt>().0, WantsToDropItem { item })
                        .expect("Failed to drop item");
                    runstate = RunState::PlayerTurn;
                }
                _ => {}
            },

            RunState::ShowUnequipMenu => match gui::unequip_menu_input(&mut self.ecs, term) {
                (gui::ItemMenuResult::Cancel, _) => {
                    runstate = RunState::AwaitingInput;
                }
                (gui::ItemMenuResult::Selected, Some(item)) => {
                    self.ecs
                        .write_storage()
                        .insert(self.ecs.fetch::<PlayerEnt>().0, WantsToRemoveItem { item })
                        .expect("Failed to remove item");
                    runstate = RunState::PlayerTurn;
                }
                _ => {}
            },

            // Don't do anything for these
            RunState::ShowTargeting { range, item } => {
                match gui::ranged_target_input(&mut self.ecs, term, range) {
                    (gui::ItemMenuResult::Cancel, _) => {
                        runstate = RunState::AwaitingInput;
                    }
                    (gui::ItemMenuResult::Selected, Some(point)) => {
                        self.ecs
                            .write_storage()
                            .insert(
                                self.ecs.fetch::<PlayerEnt>().0,
                                WantsToUseItem {
                                    item,
                                    target: Some(point),
                                },
                            )
                            .expect("Failed to use item with target");
                        runstate = RunState::PlayerTurn;
                    }
                    _ => {}
                }
            }
        }

        // Begin drawing
        systems::cull_dead_particles(&mut self.ecs, term);

        *self.ecs.write_resource() = runstate;

        None
    }
    fn draw(&self, term: &mut bracket_terminal::prelude::BTerm) {
        let mut draw_batch = DrawBatch::new();
        let runstate = *self.ecs.fetch::<RunState>();

        if SHOW_MAPGEN_VISUALIZER && runstate == RunState::MapGen {
            gui::draw_map(&self.mapgen_history[self.mapgen_index], &mut draw_batch);
            draw_batch.submit(0).unwrap();
            return;
        }

        gui::draw_map(&self.ecs.fetch::<Map>(), &mut draw_batch);
        gui::draw_ui(&self.ecs, &mut draw_batch);

        {
            let positions = self.ecs.read_storage::<Position>();
            let renderables = self.ecs.read_storage::<Renderable>();
            let hiddens = self.ecs.read_storage::<Hidden>();
            let map = self.ecs.fetch::<Map>();

            let mut data = (&positions, &renderables, !&hiddens)
                .join()
                .collect::<Vec<_>>();
            data.sort_by(|a, b| b.1.render_order.cmp(&a.1.render_order));

            for (pos, render, _) in &data {
                if map.is_visible(pos.x, pos.y) {
                    draw_batch.set(
                        (pos.x, pos.y).into(),
                        ColorPair::new(render.fg, render.bg),
                        render.glyph,
                    );
                }
            }
        }

        // Popup menus
        match runstate {
            RunState::ShowInventory => {
                gui::draw_inventory(&self.ecs, &mut draw_batch, "Inventory");
            }
            RunState::ShowDropMenu => {
                gui::draw_inventory(&self.ecs, &mut draw_batch, "Drop which item?");
            }
            RunState::ShowUnequipMenu => {
                gui::draw_unequip_menu(&self.ecs, &mut draw_batch);
            }
            RunState::ShowTargeting { range, .. } => {
                gui::draw_ranged_target(&self.ecs, term, &mut draw_batch, range);
                gui::draw_tooltips(&self.ecs, term, &mut draw_batch);
            }
            _ => {
                gui::draw_tooltips(&self.ecs, term, &mut draw_batch);
            }
        }
        draw_batch.submit(0).unwrap();
    }
}

impl PlayState {
    pub fn new_game() -> PlayState {
        PlayState::new_game_seeded(RandomNumberGenerator::new().next_u64())
    }

    pub fn new_game_seeded(seed: u64) -> PlayState {
        crate::gamelog::clear_log();
        Logger::new()
            .append("Welcome to")
            .npc_name("Legendary Spork")
            .log();

        let mut state = PlayState {
            dispatcher: crate::systems::new_dispatcher(),
            ecs: World::new(),
            mapgen_history: vec![],
            mapgen_index: 0,
            mapgen_timer: 0.,
        };
        crate::component::register_all(&mut state.ecs);

        let player_ent = spawner::player(&mut state.ecs, 0, 0);
        state.ecs.insert(PlayerEnt(player_ent));

        let rng = RandomNumberGenerator::seeded(seed);
        state.ecs.insert(rng);

        state.generate_level(1);

        if SHOW_MAPGEN_VISUALIZER {
            *state.ecs.fetch_mut::<RunState>() = RunState::MapGen;
        }

        state
    }

    pub fn load_game() -> PlayState {
        crate::gamelog::clear_log();
        Logger::new()
            .append("Welcome to")
            .npc_name("Legendary Spork")
            .log();

        let mut state = PlayState {
            dispatcher: crate::systems::new_dispatcher(),
            ecs: World::new(),
            mapgen_history: vec![],
            mapgen_index: 0,
            mapgen_timer: 0.,
        };
        crate::component::register_all(&mut state.ecs);

        saveload::load_game_from_file(&mut state.ecs);

        let rng = RandomNumberGenerator::new();
        state.ecs.insert(rng);

        state
    }

    fn goto_next_level(&mut self) {
        let to_delete = self.entities_to_remove_on_level_change();

        self.ecs
            .delete_entities(&to_delete)
            .expect("Failed to delete entities on level change");

        let depth = {
            let depth = self.ecs.read_resource::<Map>().depth;
            depth + 1
        };

        self.generate_level(depth);

        Logger::new().append("You descend to the next level").log();
    }

    fn entities_to_remove_on_level_change(&self) -> Vec<Entity> {
        let entities = self.ecs.entities();
        let players = self.ecs.read_storage::<Player>();
        let incontainers = self.ecs.read_storage::<InContainer>();
        let equipped = self.ecs.read_storage::<Equipped>();
        let player_ent = self.ecs.fetch::<PlayerEnt>().0;

        entities
            .join()
            .filter(|e| players.get(*e).is_none())
            .filter(|e| {
                if let Some(InContainer { container, .. }) = incontainers.get(*e) {
                    *container != player_ent
                } else {
                    true
                }
            })
            .filter(|e| {
                if let Some(Equipped { owner, .. }) = equipped.get(*e) {
                    *owner != player_ent
                } else {
                    true
                }
            })
            .collect()
    }

    fn generate_level(&mut self, depth: i32) {
        let mut builder = {
            let mut rng = self.ecs.write_resource::<RandomNumberGenerator>();
            map_builders::random_builder(&mut rng, depth)
        };

        crate::debug::timer("mapgen", || {
            builder.build_map();
            builder.spawn_entities(&mut self.ecs);
        });

        let Position { x: px, y: py } = builder.get_starting_position();
        let player_ent = self.ecs.fetch::<PlayerEnt>().0;
        {
            let mut positions = self.ecs.write_storage::<Position>();
            *positions.get_mut(player_ent).unwrap() = Position { x: px, y: py };
        }
        self.ecs.insert(PlayerPos((px, py).into()));

        if let Some(mut vs) = self.ecs.write_storage::<Viewshed>().get_mut(player_ent) {
            vs.dirty = true;
        }

        self.ecs.insert(builder.get_map());
        self.mapgen_history = builder.get_snapshot_history().clone();
        self.mapgen_index = 0;
        self.mapgen_timer = 0.0;
    }
}
