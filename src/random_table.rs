use bracket_random::prelude::RandomNumberGenerator;

struct RandomEntry {
    name: String,
    weight: i32,
}

impl RandomEntry {
    pub fn new<S: ToString>(name: S, weight: i32) -> RandomEntry {
        RandomEntry {
            name: name.to_string(),
            weight,
        }
    }
}

pub struct RandomTable {
    entries: Vec<RandomEntry>,
    total_weight: i32,
}

impl RandomTable {
    pub fn _new() -> RandomTable {
        RandomTable {
            entries: vec![],
            total_weight: 0,
        }
    }

    pub fn new_with_entries(entries: &[(&str, i32)]) -> RandomTable {
        let mut total_weight = 0;
        let entries = entries
            .iter()
            .filter_map(|(name, weight)| {
                if *weight > 0 {
                    total_weight += weight;
                    Some(RandomEntry::new(name, *weight))
                } else {
                    None
                }
            })
            .collect();
        RandomTable {
            total_weight,
            entries,
        }
    }

    pub fn _add<S: ToString>(mut self, name: S, weight: i32) -> Self {
        if weight > 0 {
            self.total_weight += weight;
            self.entries.push(RandomEntry::new(name, weight));
        }
        self
    }

    pub fn roll<'a>(&'a self, rng: &mut RandomNumberGenerator) -> Option<&'a str> {
        if self.total_weight == 0 {
            return None;
        }

        let mut roll = rng.range(0, self.total_weight);
        for entry in &self.entries {
            if roll < entry.weight {
                return Some(&entry.name);
            }
            roll -= entry.weight;
        }

        None
    }
}
