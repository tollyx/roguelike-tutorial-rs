use bracket_color::prelude::RGB;
use bracket_geometry::prelude::Point;
use serde::{Deserialize, Serialize};
#[allow(deprecated)]
use specs::error::NoError;
#[warn(deprecated)]
use specs::prelude::*;
use specs::saveload::{ConvertSaveload, Marker, SimpleMarker, SimpleMarkerAllocator};
use specs_derive::*;

#[derive(ConvertSaveload, Clone)]
pub struct PlayerPos(pub Point);

#[derive(ConvertSaveload, Clone)]
pub struct PlayerEnt(pub Entity);

pub struct SerializeMe;

#[derive(Component, ConvertSaveload, Clone)]
pub struct SerializationHelper {
    pub map: crate::map::Map,
}

pub fn register_all(ecs: &mut World) {
    ecs.register::<AreaOfEffect>();
    ecs.register::<BlocksTile>();
    ecs.register::<CombatStats>();
    ecs.register::<Confusion>();
    ecs.register::<Consumable>();
    ecs.register::<Container>();
    ecs.register::<DefenseBonus>();
    ecs.register::<EntityMoved>();
    ecs.register::<EntryTrigger>();
    ecs.register::<Equippable>();
    ecs.register::<Equipped>();
    ecs.register::<Hidden>();
    ecs.register::<HungerClock>();
    ecs.register::<InContainer>();
    ecs.register::<InflictsDamage>();
    ecs.register::<Invulnerable>();
    ecs.register::<Item>();
    ecs.register::<MagicMapper>();
    ecs.register::<MeleePowerBonus>();
    ecs.register::<Monster>();
    ecs.register::<Name>();
    ecs.register::<ParticleLifetime>();
    ecs.register::<Player>();
    ecs.register::<Position>();
    ecs.register::<ProvidesFood>();
    ecs.register::<ProvidesHealing>();
    ecs.register::<Ranged>();
    ecs.register::<Renderable>();
    ecs.register::<SerializationHelper>();
    ecs.register::<SimpleMarker<SerializeMe>>();
    ecs.register::<SingleActivation>();
    ecs.register::<SufferDamage>();
    ecs.register::<Viewshed>();
    ecs.register::<WantsToDropItem>();
    ecs.register::<WantsToMelee>();
    ecs.register::<WantsToPickupItem>();
    ecs.register::<WantsToRemoveItem>();
    ecs.register::<WantsToUseItem>();

    // neccessary game init
    ecs.insert(SimpleMarkerAllocator::<SerializeMe>::new());
    ecs.insert(crate::gamestate::playstate::RunState::PreRun);
    ecs.insert(crate::systems::ParticleBuilder::new());
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}

impl From<(i32, i32)> for Position {
    fn from((x, y): (i32, i32)) -> Self {
        Position { x, y }
    }
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct Renderable {
    pub glyph: u16,
    pub fg: RGB,
    pub bg: RGB,
    pub render_order: i32,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct Name {
    pub name: String,
}

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct Player;

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct Monster;

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct BlocksTile;

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct Container;

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct Hidden;

#[derive(Component, ConvertSaveload, Clone)]
pub struct InContainer {
    pub container: Entity,
}

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct Item;

#[derive(Component, ConvertSaveload, Clone)]
pub struct ProvidesHealing {
    pub heal_amount: i32,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct Ranged {
    pub range: i32,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct AreaOfEffect {
    pub radius: i32,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct InflictsDamage {
    pub damage: i32,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct ParticleLifetime {
    pub lifetime_ms: f32,
}

#[derive(Serialize, Deserialize, PartialEq, Clone, Copy)]
pub enum EquipmentSlot {
    Melee,
    Shield,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct Equippable {
    pub slot: EquipmentSlot,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct Equipped {
    pub owner: Entity,
    pub slot: EquipmentSlot,
}

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct EntryTrigger;

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct EntityMoved;

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct SingleActivation;

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct Invulnerable;

#[derive(Component, ConvertSaveload, Clone)]
pub struct Confusion {
    pub turns: i32,
}

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct Consumable;

#[derive(Component, ConvertSaveload, Clone)]
pub struct CombatStats {
    pub hp_max: i32,
    pub hp: i32,
    pub defense: i32,
    pub power: i32,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct WantsToMelee {
    pub target: Entity,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct WantsToPickupItem {
    pub item: Entity,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct WantsToUseItem {
    pub item: Entity,
    pub target: Option<Point>,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct WantsToDropItem {
    pub item: Entity,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct WantsToRemoveItem {
    pub item: Entity,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct MeleePowerBonus {
    pub power: i32,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct DefenseBonus {
    pub defense: i32,
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct SufferDamage {
    pub amount: Vec<i32>,
}

impl SufferDamage {
    pub fn new_damage(store: &mut WriteStorage<SufferDamage>, victim: Entity, amount: i32) {
        if let Some(suffering) = store.get_mut(victim) {
            suffering.amount.push(amount)
        } else {
            store
                .insert(
                    victim,
                    SufferDamage {
                        amount: vec![amount],
                    },
                )
                .expect("Unable to insert damage");
        }
    }
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct Viewshed {
    pub visible_tiles: Vec<Point>,
    pub range: i32,
    pub dirty: bool,
}
impl Viewshed {
    pub fn new(range: i32) -> Viewshed {
        Viewshed {
            visible_tiles: vec![],
            range,
            dirty: true,
        }
    }
}

#[derive(Component, Serialize, Deserialize, Copy, Clone, PartialEq)]
pub enum HungerState {
    WellFed,
    Sated,
    Hungry,
    Starving,
}

impl HungerState {
    pub fn allows_passive_healing(self) -> bool {
        match self {
            HungerState::WellFed | HungerState::Sated => true,
            HungerState::Hungry | HungerState::Starving => false,
        }
    }
}

#[derive(Component, ConvertSaveload, Clone)]
pub struct HungerClock {
    pub state: HungerState,
    pub duration: i32,
}

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct ProvidesFood;

#[derive(Component, Serialize, Deserialize, Clone, Default)]
#[storage(NullStorage)]
pub struct MagicMapper;
