use crate::{
    component::{
        CombatStats, Equipped, HungerClock, HungerState, InContainer, Name, Player, PlayerEnt,
        PlayerPos, Position, Viewshed,
    },
    map::{Map, TileType},
    rect::Rect,
    WINDOW_HEIGHT, WINDOW_WIDTH,
};
use bracket_color::prelude::*;
use bracket_geometry::prelude::Point;
use bracket_geometry::prelude::Rect as BRect;
use bracket_pathfinding::prelude::DistanceAlg;
use bracket_terminal::prelude::{
    letter_to_option, to_cp437, BTerm, DrawBatch, FontCharType, TextBlock, VirtualKeyCode,
};
use specs::prelude::*;

pub fn draw_map(map: &Map, draw: &mut DrawBatch) {
    for y in 0..map.height() {
        for x in 0..map.width() {
            if map.is_revealed(x, y) {
                let glyph;
                let mut fg;
                let mut bg;
                match map.get_tile(x, y) {
                    TileType::Floor => {
                        fg = RGB::named(GRAY);
                        bg = RGB::named(BLACK);
                        glyph = to_cp437('.');
                    }
                    TileType::Wall => {
                        fg = RGB::named(BLACK);
                        bg = RGB::named(WHITE);
                        glyph = crate::map::wall_glyph(&map, x, y);
                    }
                    TileType::DownStairs => {
                        fg = RGB::named(BLACK);
                        bg = RGB::named(CYAN);
                        glyph = to_cp437('>');
                    }
                }

                if !map.is_visible(x, y) {
                    fg = fg * 0.25f32;
                    bg = bg * 0.25f32;
                } else if map.is_bloody(x, y) {
                    bg = RGB::from_f32(0.70, 0., 0.);
                }

                draw.set((x as i32, y as i32).into(), ColorPair::new(fg, bg), glyph);
            }
        }
    }
}

pub fn draw_ui(ecs: &World, batch: &mut DrawBatch) {
    let info_rect = Rect::new(0, WINDOW_HEIGHT - 9, WINDOW_WIDTH - 1, 8);
    let color_regular = ColorPair::new(RGB::named(WHITE), RGB::named(BLACK));
    let color_hilight = ColorPair::new(RGB::named(YELLOW), RGB::named(BLACK));
    batch.draw_box(info_rect.clone().into(), color_regular);

    let combat_stats = ecs.read_storage::<CombatStats>();
    let players = ecs.read_storage::<Player>();
    let hunger_clocks = ecs.read_storage::<HungerClock>();
    let map = ecs.fetch::<Map>();
    let mut left_pos = info_rect.left + 1;
    let depth_text = format!("Depth {}", map.depth);
    batch.print_color((left_pos, info_rect.top).into(), &depth_text, color_hilight);

    left_pos += depth_text.len() as i32;

    for (_player, stats, hc) in (&players, &combat_stats, &hunger_clocks).join() {
        batch.print_color(
            (left_pos + 1, info_rect.top).into(),
            &format!("[HP: {:02} / {:02}]", stats.hp, stats.hp_max),
            color_hilight,
        );
        match hc.state {
            HungerState::WellFed => {
                let text = "Well Fed";
                batch.print_color(
                    (info_rect.right - text.len() as i32, info_rect.top).into(),
                    text,
                    ColorPair::new(RGB::named(GREEN), RGB::named(BLACK)),
                );
            }
            HungerState::Sated => {
                let text = "Sated";
                batch.print_color(
                    (info_rect.right - text.len() as i32, info_rect.top).into(),
                    text,
                    ColorPair::new(RGB::named(WHITE), RGB::named(BLACK)),
                );
            }
            HungerState::Hungry => {
                let text = "Hungry";
                batch.print_color(
                    (info_rect.right - text.len() as i32, info_rect.top).into(),
                    text,
                    ColorPair::new(RGB::named(ORANGE), RGB::named(BLACK)),
                );
            }
            HungerState::Starving => {
                let text = "Starving";
                batch.print_color(
                    (info_rect.right - text.len() as i32, info_rect.top).into(),
                    text,
                    ColorPair::new(RGB::named(RED), RGB::named(BLACK)),
                );
            }
        }
    }

    let mut block = TextBlock::new(
        info_rect.left + 1,
        info_rect.top + 1,
        info_rect.width() - 1,
        info_rect.height() - 1,
    );
    _ = block.print(&crate::gamelog::log_display(
        (info_rect.height() - 1) as usize,
    ));
    block.render_to_draw_batch(batch);
}

pub fn draw_tooltips(ecs: &World, ctx: &BTerm, draw: &mut DrawBatch) {
    let map = ecs.fetch::<Map>();
    let names = ecs.read_storage::<Name>();
    let positions = ecs.read_storage::<Position>();

    let color = ColorPair::new(RGB::named(BLACK), RGB::named(DARK_GRAY));

    let (mx, my) = ctx.mouse_pos();
    if !map.is_visible(mx, my) {
        return;
    }

    let mut tooltip: Vec<String> = vec![];
    for (name, pos) in (&names, &positions).join() {
        if pos.x == mx && pos.y == my {
            tooltip.push(name.name.to_string());
        }
    }
    if !tooltip.is_empty() {
        let w = tooltip.iter().map(|s| s.len() as i32).max().unwrap() + 3;
        if mx > w {
            let arrow_pos = Point::new(mx - 2, my);
            let left_x = mx - w;
            let mut y = my;
            for s in &tooltip {
                draw.print_color((left_x, y).into(), s, color);
                let padding = (w - s.len() as i32) - 1;
                for i in 0..padding {
                    draw.print_color((arrow_pos.x - i, y).into(), &" ".to_string(), color);
                }
                y += 1;
            }
            draw.print_color(arrow_pos, &"->".to_string(), color);
        } else {
            let arrow_pos = Point::new(mx + 1, my);
            let left_x = mx + 3;
            let mut y = my;
            for s in &tooltip {
                draw.print_color((left_x + 1, y).into(), s, color);
                let padding = (w - s.len() as i32) - 1;
                for i in 0..padding {
                    draw.print_color((arrow_pos.x + 1 + i, y).into(), &" ".to_string(), color);
                }
                y += 1;
            }
            draw.print_color((arrow_pos.x, arrow_pos.y).into(), &"<-".to_string(), color);
        }
    }
}

pub enum ItemMenuResult {
    Cancel,
    NoResponse,
    Selected,
}

pub fn inventory_input(ecs: &World, ctx: &BTerm) -> (ItemMenuResult, Option<Entity>) {
    let player_ent = ecs.fetch::<PlayerEnt>().0;
    let names = ecs.read_storage::<Name>();
    let backpack = ecs.read_storage::<InContainer>();
    let entities = ecs.entities();

    let count = (&backpack, &names)
        .join()
        .filter(|(b, _n)| b.container == player_ent)
        .count();
    let mut usable = (&entities, &backpack, &names)
        .join()
        .filter(|(_e, b, _n)| b.container == player_ent)
        .map(|(e, _, _)| e);

    match ctx.key {
        None => (ItemMenuResult::NoResponse, None),
        Some(VirtualKeyCode::Escape) => (ItemMenuResult::Cancel, None),
        Some(key) => {
            let selection = letter_to_option(key);
            if (0..count as i32).contains(&selection) {
                (ItemMenuResult::Selected, usable.nth(selection as usize))
            } else {
                (ItemMenuResult::NoResponse, None)
            }
        }
    }
}

pub fn draw_inventory(ecs: &World, draw: &mut DrawBatch, text: &str) {
    let player_ent = ecs.fetch::<PlayerEnt>().0;
    let names = ecs.read_storage::<Name>();
    let backpack = ecs.read_storage::<InContainer>();

    let color_regular = ColorPair::new(RGB::named(WHITE), RGB::named(BLACK));
    let color_hilight = ColorPair::new(RGB::named(YELLOW), RGB::named(BLACK));

    let count = (&backpack, &names)
        .join()
        .filter(|(b, _n)| b.container == player_ent)
        .count();

    let mut y = (25 - (count / 2)) as i32;
    draw.draw_box(
        BRect::with_size(15, y - 2, 31, (count + 3) as i32),
        color_regular,
    );
    draw.print_color((18, y - 2).into(), text, color_hilight);
    draw.print_color(
        (18, y + count as i32 + 1).into(),
        "ESCAPE to cancel",
        color_hilight,
    );

    for (j, (_pack, name)) in (&backpack, &names)
        .join()
        .filter(|(b, _n)| b.container == player_ent).enumerate()
    {
        draw.set((17, y).into(), color_regular, to_cp437('('));
        draw.set((18, y).into(), color_hilight, 97 + j as u16);
        draw.set((19, y).into(), color_regular, to_cp437(')'));

        draw.print((21, y).into(), &name.name.to_string());
        y += 1;
    }
}

pub fn unequip_menu_input(ecs: &mut World, ctx: &BTerm) -> (ItemMenuResult, Option<Entity>) {
    let player_entity = ecs.fetch::<PlayerEnt>().0;
    let names = ecs.read_storage::<Name>();
    let equipped = ecs.read_storage::<Equipped>();
    let entities = ecs.entities();

    let count = (&equipped, &names)
        .join()
        .filter(|(eq, _)| eq.owner == player_entity)
        .count();

    let mut equippable = (&entities, &equipped, &names)
        .join()
        .filter(|(_, eq, _)| eq.owner == player_entity)
        .map(|(e, _, _)| e);

    match ctx.key {
        None => (ItemMenuResult::NoResponse, None),
        Some(VirtualKeyCode::Escape) => (ItemMenuResult::Cancel, None),
        Some(key) => {
            let selection = letter_to_option(key);
            if selection > -1 && selection < count as i32 {
                return (ItemMenuResult::Selected, equippable.nth(selection as usize));
            }
            (ItemMenuResult::NoResponse, None)
        }
    }
}

pub fn draw_unequip_menu(ecs: &World, draw: &mut DrawBatch) {
    let player_entity = ecs.fetch::<PlayerEnt>().0;
    let names = ecs.read_storage::<Name>();
    let equipped = ecs.read_storage::<Equipped>();

    let color_regular = ColorPair::new(RGB::named(WHITE), RGB::named(BLACK));
    let color_hilight = ColorPair::new(RGB::named(YELLOW), RGB::named(BLACK));

    let count = (&equipped, &names)
        .join()
        .filter(|(eq, _)| eq.owner == player_entity)
        .count();

    let mut y = (25 - (count / 2)) as i32;
    draw.draw_box(
        BRect::with_size(15, y - 2, 31, (count + 3) as i32),
        color_regular,
    );
    draw.print_color((18, y - 2).into(), "Remove Which Item?", color_hilight);
    draw.print_color(
        (18, y + count as i32 + 1).into(),
        "ESCAPE to cancel",
        color_regular,
    );

    for (j, (_, name)) in (&equipped, &names)
        .join()
        .filter(|(eq, _)| eq.owner == player_entity).enumerate()
    {
        draw.set((17, y).into(), color_regular, to_cp437('('));
        draw.set((18, y).into(), color_hilight, 97 + j as FontCharType);
        draw.set((19, y).into(), color_regular, to_cp437(')'));

        draw.print((21, y).into(), &name.name.to_string());
        y += 1;
    }
}

pub fn ranged_target_input(
    ecs: &mut World,
    ctx: &BTerm,
    range: i32,
) -> (ItemMenuResult, Option<Point>) {
    let player_entity = ecs.fetch::<PlayerEnt>().0;
    let player_pos = ecs.fetch::<PlayerPos>().0;
    let viewsheds = ecs.read_storage::<Viewshed>();

    let available_cells = if let Some(visible) = viewsheds.get(player_entity) {
        visible
            .visible_tiles
            .iter()
            .filter(|x| DistanceAlg::Pythagoras.distance2d(player_pos, **x) <= range as f32)
            .copied()
            .collect::<Vec<Point>>()
    } else {
        return (ItemMenuResult::Cancel, None);
    };

    let mpos = ctx.mouse_pos().into();

    // If the mouse pos is a valid target
    if ctx.left_click {
        if available_cells.contains(&mpos) {
            (ItemMenuResult::Selected, Some(mpos))
        } else {
            (ItemMenuResult::Cancel, None)
        }
    } else {
        (ItemMenuResult::NoResponse, None)
    }
}

pub fn draw_ranged_target(ecs: &World, ctx: &BTerm, draw: &mut DrawBatch, range: i32) {
    let player_entity = ecs.fetch::<PlayerEnt>().0;
    let player_pos = ecs.fetch::<PlayerPos>().0;
    let viewsheds = ecs.read_storage::<Viewshed>();

    let color_hilight = ColorPair::new(RGB::named(YELLOW), RGB::named(BLACK));

    draw.print_color((5, 0).into(), "Select Target:", color_hilight);
    let mpos = ctx.mouse_pos().into();

    // Highlight available target cells
    let mut available_cells = Vec::new();
    if let Some(visible) = viewsheds.get(player_entity) {
        // We have a viewshed
        for pos in &visible.visible_tiles {
            let distance = DistanceAlg::Pythagoras.distance2d(player_pos, *pos);
            if distance <= range as f32 {
                draw.set_bg(*pos, RGB::named(BLUE));
                available_cells.push(pos);
            }
        }
    }
    // If the mouse pos is a valid target
    if available_cells.contains(&&mpos) {
        draw.set_bg(mpos, RGB::named(YELLOW));
    } else {
        draw.set_bg(mpos, RGB::named(RED));
    }
}
