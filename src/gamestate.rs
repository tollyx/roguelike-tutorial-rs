pub mod mainmenu;
pub mod playstate;

use bracket_terminal::prelude::{render_draw_buffer, BTerm};

pub enum GameStateAction {
    Pop,
    Push(Box<dyn GameState>),
    #[allow(dead_code)]
    Swap(Box<dyn GameState>),
}

pub trait GameState {
    fn update(&mut self, term: &BTerm) -> Option<GameStateAction>;

    fn draw(&self, term: &mut BTerm);

    /// Unimplemented!
    fn update_previous(&self) -> bool {
        false
    }

    /// Unimplemented!
    fn draw_previous(&self) -> bool {
        false
    }
}

pub struct GameStateManager {
    states: Vec<Box<dyn GameState>>,
}

impl GameStateManager {
    pub fn new(initial_state: Box<dyn GameState>) -> GameStateManager {
        GameStateManager {
            states: vec![initial_state],
        }
    }

    pub fn update(&mut self, term: &mut BTerm) {
        let result = if let Some(state) = self.states.last_mut() {
            state.update(term)
        } else {
            term.quit();
            None
        };
        if let Some(r) = result {
            match r {
                GameStateAction::Pop => {
                    self.states.pop();
                }
                GameStateAction::Push(state) => self.states.push(state),
                GameStateAction::Swap(state) => {
                    self.states.pop();
                    self.states.push(state);
                }
            }
        }
    }

    pub fn draw(&self, term: &mut BTerm) {
        term.cls();
        if let Some(state) = self.states.last() {
            state.draw(term);
        }
        render_draw_buffer(term).expect("Failed to render the draw buffer");
    }
}
