use super::{common, MapBuilder};
use crate::{
    component::Position,
    map::{Map, TileType},
    rect::Rect,
    spawner, SHOW_MAPGEN_VISUALIZER, WINDOW_HEIGHT, WINDOW_WIDTH,
};
use bracket_random::prelude::RandomNumberGenerator;

const MAX_ROOM_SIZE: i32 = 12;
const MIN_ROOM_SIZE: i32 = 4;

pub struct BspDungeonBuilder {
    rng: RandomNumberGenerator,
    map: Map,
    start_pos: Position,
    depth: i32,
    rooms: Vec<Rect>,
    history: Vec<Map>,
    rects: Vec<Rect>,
}

impl BspDungeonBuilder {
    pub fn new(seed: u64, depth: i32) -> BspDungeonBuilder {
        let mut map = Map::new(WINDOW_WIDTH, WINDOW_HEIGHT - 9, TileType::Wall);
        map.depth = depth;
        BspDungeonBuilder {
            rng: RandomNumberGenerator::seeded(seed),
            start_pos: Position { x: 0, y: 0 },
            depth,
            map,
            rooms: vec![],
            history: vec![],
            rects: vec![],
        }
    }

    fn take_snapshot(&mut self) {
        if SHOW_MAPGEN_VISUALIZER {
            let mut map = self.map.clone();
            map.reveal_everything();
            self.history.push(map);
        }
    }

    fn add_subrects(&mut self, rect: &Rect) {
        let (w, h) = (rect.width(), rect.height());

        if w > h {
            let hw = (w / 2).max(1);
            self.rects.push(Rect::new(rect.left, rect.top, hw, h));
            self.rects.push(Rect::new(rect.left + hw, rect.top, hw, h));
        } else {
            let hh = (h / 2).max(1);
            self.rects.push(Rect::new(rect.left, rect.top, w, hh));
            self.rects.push(Rect::new(rect.left, rect.top + hh, w, hh));
        }
    }

    fn get_random_rect(&mut self) -> Rect {
        *self.rng.random_slice_entry(&self.rects).unwrap()
    }

    fn get_random_sub_rect(&mut self, rect: &Rect) -> Rect {
        let mut result = *rect;
        let (rw, rh) = (rect.width(), rect.height());

        let w = self.rng.range(MIN_ROOM_SIZE, rw.min(MAX_ROOM_SIZE));
        let h = self.rng.range(MIN_ROOM_SIZE, rh.min(MAX_ROOM_SIZE));

        result.left += self.rng.range(0, rw - w);
        result.top += self.rng.range(0, rh - h);
        result.right = result.left + w;
        result.bottom = result.top + h;

        result
    }

    fn is_possible(&self, rect: &Rect) -> bool {
        let mut expanded = *rect;
        expanded.left -= 2;
        expanded.top -= 2;
        expanded.right += 2;
        expanded.bottom += 2;

        for x in expanded.left..=expanded.right {
            for y in expanded.top..=expanded.bottom {
                if !self.map.is_inside_bounds(x, y) {
                    return false;
                }
                if self.map.get_tile(x, y) != TileType::Wall {
                    return false;
                }
            }
        }

        true
    }

    fn build(&mut self) {
        self.rects.clear();
        let first_room = Rect::new(1, 1, self.map.width() - 2, self.map.height() - 2);
        self.rects.push(first_room);
        self.add_subrects(&first_room);

        for _ in 0..240 {
            let rect = self.get_random_rect();
            let candidate = self.get_random_sub_rect(&rect);

            if self.is_possible(&candidate) {
                common::apply_room(&mut self.map, &candidate);
                self.rooms.push(candidate);
                self.add_subrects(&rect);
                self.take_snapshot();
            }
        }

        match self.rng.range(0, 6) {
            0 => self.rooms.sort_by(|a, b| a.left.cmp(&b.left)),
            1 => self.rooms.sort_by(|a, b| a.top.cmp(&b.top)),
            2 => self.rooms.sort_by(|a, b| a.right.cmp(&b.right).reverse()),
            3 => self.rooms.sort_by(|a, b| a.bottom.cmp(&b.bottom).reverse()),
            _ => {
                // shuffle rooms for more excitement
                for _ in 0..(self.rooms.len() * 2) {
                    let (a, b) = (
                        self.rng.range(0, self.rooms.len()),
                        self.rng.range(0, self.rooms.len()),
                    );
                    self.rooms.swap(a, b);
                }
            }
        }

        for i in 0..self.rooms.len() - 1 {
            let room = &self.rooms[i];
            let room_next = &self.rooms[i + 1];
            let sx = self.rng.range(room.left, room.right);
            let sy = self.rng.range(room.top, room.bottom);

            let ex = self.rng.range(room_next.left, room_next.right);
            let ey = self.rng.range(room_next.top, room_next.bottom);

            self.draw_corridor(sx, sy, ex, ey);
            self.take_snapshot();
        }

        let (x, y) = self.rooms[0].center();
        self.start_pos = Position { x, y };

        let end_room = &self.rooms[self.rooms.len() - 1];
        let ex = self.rng.range(end_room.left, end_room.right);
        let ey = self.rng.range(end_room.top, end_room.bottom);
        self.map.set_tile(ex, ey, TileType::DownStairs);
    }

    fn draw_corridor(&mut self, sx: i32, sy: i32, ex: i32, ey: i32) {
        // if self.rng.range(0, 2) == 0 {
        //     apply_tunnel_x(&mut self.map, sx, ex, ey);
        //     apply_tunnel_y(&mut self.map, sy, ey, ex);
        // } else {
        //     apply_tunnel_y(&mut self.map, sy, ey, ex);
        //     apply_tunnel_x(&mut self.map, sx, ex, ey);
        // }
        common::apply_tunnel_xy_rng(&mut self.rng, &mut self.map, sx, sy, ex, ey)
    }
}

impl MapBuilder for BspDungeonBuilder {
    fn build_map(&mut self) {
        self.build()
    }
    fn spawn_entities(&mut self, ecs: &mut specs::World) {
        for room in self.rooms.iter().skip(1) {
            spawner::populate_room(&mut self.rng, ecs, room, self.depth);
        }
    }
    fn get_map(&mut self) -> crate::map::Map {
        self.map.clone()
    }
    fn get_starting_position(&mut self) -> crate::component::Position {
        self.start_pos.clone()
    }
    fn get_snapshot_history(&self) -> &Vec<crate::map::Map> {
        &self.history
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn consistency_check() {
        let mut builder = BspDungeonBuilder::new(42, 1);
        builder.build_map();
        let map1 = builder.get_map();

        let mut builder = BspDungeonBuilder::new(42, 1);
        builder.build_map();
        let map2 = builder.get_map();
        for y in 0..map1.height() {
            for x in 0..map1.width() {
                assert_eq!(
                    map1.get_tile(x, y),
                    map2.get_tile(x, y),
                    "checking position ({},{})",
                    x,
                    y
                );
            }
        }
    }
}
