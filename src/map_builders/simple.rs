use super::{
    common::{apply_room, apply_tunnel_xy_rng},
    MapBuilder,
};
use crate::{
    component::Position,
    map::{Map, TileType},
    rect::Rect,
    spawner, WINDOW_HEIGHT, WINDOW_WIDTH,
};
use bracket_random::prelude::RandomNumberGenerator;

pub struct SimpleMapBuilder {
    rng: RandomNumberGenerator,
    map: Map,
    depth: i32,
    rooms: Vec<Rect>,
    start_pos: Position,
    history: Vec<Map>,
}

impl SimpleMapBuilder {
    pub fn new(seed: u64, depth: i32) -> Self {
        let mut map = Map::new(WINDOW_WIDTH, WINDOW_HEIGHT - 8, TileType::Wall);
        map.depth = depth;
        SimpleMapBuilder {
            rng: RandomNumberGenerator::seeded(seed),
            start_pos: Position { x: 0, y: 0 },
            depth,
            map,
            rooms: vec![],
            history: vec![],
        }
    }

    pub fn rooms_and_corridors(&mut self) {
        const MAX_ROOMS: i32 = 30;
        const MIN_SIZE: i32 = 6;
        const MAX_SIZE: i32 = 10;

        for _ in 0..MAX_ROOMS {
            let w = self.rng.range(MIN_SIZE, MAX_SIZE);
            let h = self.rng.range(MIN_SIZE, MAX_SIZE);
            let x = self.rng.range(1, self.map.width() - w - 1);
            let y = self.rng.range(1, self.map.height() - h - 1);

            let new_room = Rect::new(x, y, w, h);

            if self.rooms.iter().all(|r| !r.intersects(&new_room)) {
                apply_room(&mut self.map, &new_room);

                if !self.rooms.is_empty() {
                    let (new_x, new_y) = new_room.center();
                    let (prev_x, prev_y) = self.rooms[self.rooms.len() - 1].center();
                    apply_tunnel_xy_rng(&mut self.rng, &mut self.map, prev_x, prev_y, new_x, new_y);
                }
                self.rooms.push(new_room);
                self.take_snapshot();
            }
        }
        let (exit_x, exit_y) = self.rooms.iter().last().unwrap().center();
        self.map.set_tile(exit_x, exit_y, TileType::DownStairs);
        self.map.populate_blocked();
        let (x, y) = self.rooms[0].center();
        self.start_pos = Position { x, y };
        self.take_snapshot();
    }

    fn take_snapshot(&mut self) {
        if crate::SHOW_MAPGEN_VISUALIZER {
            let mut map = self.map.clone();
            map.reveal_everything();
            self.history.push(map);
        }
    }
}

impl MapBuilder for SimpleMapBuilder {
    fn build_map(&mut self) {
        self.rooms_and_corridors();
    }
    fn spawn_entities(&mut self, ecs: &mut specs::World) {
        for room in self.rooms.iter().skip(1) {
            spawner::populate_room(&mut self.rng, ecs, room, self.depth);
        }
    }
    fn get_map(&mut self) -> Map {
        self.map.clone()
    }
    fn get_starting_position(&mut self) -> Position {
        self.start_pos.clone()
    }
    fn get_snapshot_history(&self) -> &Vec<Map> {
        &self.history
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn consistency_check() {
        let mut builder = SimpleMapBuilder::new(42, 1);
        builder.build_map();
        let map1 = builder.get_map();

        let mut builder = SimpleMapBuilder::new(42, 1);
        builder.build_map();
        let map2 = builder.get_map();
        for y in 0..map1.height() {
            for x in 0..map1.width() {
                assert_eq!(
                    map1.get_tile(x, y),
                    map2.get_tile(x, y),
                    "checking position ({},{})",
                    x,
                    y
                );
            }
        }
    }
}
