use crate::{
    map::{Map, TileType},
    rect::Rect,
};
use bracket_random::prelude::RandomNumberGenerator;

pub fn apply_room(map: &mut Map, room: &Rect) {
    for y in room.top..room.bottom {
        for x in room.left..room.right {
            map.set_tile(x, y, TileType::Floor);
        }
    }
}

pub fn _apply_tunnel_xy(map: &mut Map, sx: i32, sy: i32, ex: i32, ey: i32) {
    if (sx - ex).abs() > (sy - ey).abs() {
        apply_tunnel_x(map, sx, ex, sy);
        apply_tunnel_y(map, sy, ey, ex);
    } else {
        apply_tunnel_y(map, sy, ey, sx);
        apply_tunnel_x(map, sx, ex, ey);
    }
}

pub fn apply_tunnel_xy_rng(
    rng: &mut RandomNumberGenerator,
    map: &mut Map,
    sx: i32,
    sy: i32,
    ex: i32,
    ey: i32,
) {
    if rng.range(0, 2) == 0 {
        apply_tunnel_x(map, sx, ex, sy);
        apply_tunnel_y(map, sy, ey, ex);
    } else {
        apply_tunnel_y(map, sy, ey, sx);
        apply_tunnel_x(map, sx, ex, ey);
    }
}

pub fn apply_tunnel_x(map: &mut Map, x1: i32, x2: i32, y: i32) {
    for x in x1.min(x2)..=x1.max(x2) {
        map.set_tile(x, y, TileType::Floor)
    }
}

pub fn apply_tunnel_y(map: &mut Map, y1: i32, y2: i32, x: i32) {
    for y in y1.min(y2)..=y1.max(y2) {
        map.set_tile(x, y, TileType::Floor)
    }
}
