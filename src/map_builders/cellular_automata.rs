use super::MapBuilder;
use crate::{
    component::Position,
    map::{Map, TileType},
    spawner, SHOW_MAPGEN_VISUALIZER, WINDOW_HEIGHT, WINDOW_WIDTH,
};
use bracket_noise::prelude::{CellularDistanceFunction, FastNoise, NoiseType};
use bracket_pathfinding::prelude::DijkstraMap;
use bracket_random::prelude::RandomNumberGenerator;
use std::collections::HashMap;

/// This one is fairly slow, comparatively with the others.
/// Should find a way to speed it up somehow.

pub struct CellularAutomataBuilder {
    depth: i32,
    history: Vec<Map>,
    map: Map,
    rng: RandomNumberGenerator,
    start_pos: Position,
    noise_areas: HashMap<i32, Vec<(i32, i32)>>,
}

impl MapBuilder for CellularAutomataBuilder {
    fn build_map(&mut self) {
        self.build()
    }
    fn spawn_entities(&mut self, ecs: &mut specs::World) {
        for area in self.noise_areas.values() {
            spawner::spawn_region(&mut self.rng, ecs, area, self.depth);
        }
    }
    fn get_map(&mut self) -> Map {
        self.map.clone()
    }
    fn get_starting_position(&mut self) -> Position {
        self.start_pos.clone()
    }
    fn get_snapshot_history(&self) -> &Vec<Map> {
        &self.history
    }
}

impl CellularAutomataBuilder {
    pub fn new(seed: u64, depth: i32) -> CellularAutomataBuilder {
        let mut map = Map::new(WINDOW_WIDTH, WINDOW_HEIGHT - 9, TileType::Wall);
        map.depth = depth;
        CellularAutomataBuilder {
            depth,
            history: vec![],
            map,
            rng: RandomNumberGenerator::seeded(seed),
            start_pos: Position { x: 0, y: 0 },
            noise_areas: HashMap::new(),
        }
    }

    pub fn build(&mut self) {
        for y in 1..self.map.height() - 1 {
            for x in 1..self.map.width() - 1 {
                self.map.set_tile(
                    x,
                    y,
                    if self.rng.range(0, 100) > 55 {
                        TileType::Floor
                    } else {
                        TileType::Wall
                    },
                );
            }
        }
        self.take_snapshot();

        for _i in 0..10 {
            let mut next_map = self.map.clone();
            for y in 1..self.map.height() - 1 {
                for x in 1..self.map.width() - 1 {
                    let neigh = self
                        .map
                        .get_neightbours(x, y)
                        .iter()
                        .filter(|p| self.map.get_tile(p.x, p.y) == TileType::Wall)
                        .count();

                    if neigh > 4 || neigh == 0 {
                        next_map.set_tile(x, y, TileType::Wall);
                    } else {
                        next_map.set_tile(x, y, TileType::Floor);
                    }
                }
            }
            self.map = next_map;
            self.take_snapshot();
        }

        // Clean up single walls
        for _i in 0..1 {
            let mut next_map = self.map.clone();
            for y in 1..self.map.height() - 1 {
                for x in 1..self.map.width() - 1 {
                    let neigh = self
                        .map
                        .get_neightbours(x, y)
                        .iter()
                        .filter(|p| self.map.get_tile(p.x, p.y) == TileType::Wall)
                        .count();

                    if neigh == 0 {
                        next_map.set_tile(x, y, TileType::Floor);
                    }
                }
            }
            self.map = next_map;
            self.take_snapshot();
        }

        // Create regions using voronoi diagrams
        let mut noise = FastNoise::seeded(self.rng.next_u64());
        noise.set_noise_type(NoiseType::Cellular);
        noise.set_frequency(0.08);
        noise.set_cellular_distance_function(CellularDistanceFunction::Manhattan);
        for y in 1..self.map.height() - 1 {
            for x in 1..self.map.width() - 1 {
                if self.map.get_tile(x, y) == TileType::Floor {
                    let cell_value_f = noise.get_noise(x as f32, y as f32) * 10240.0;
                    let cell_value = cell_value_f as i32;

                    if let Some(v) = self.noise_areas.get_mut(&cell_value) {
                        v.push((x, y));
                    } else {
                        self.noise_areas.insert(cell_value, vec![(x, y)]);
                    }
                }
            }
        }

        self.start_pos = Position {
            x: self.map.width() / 2,
            y: self.map.height() / 2,
        };
        while TileType::Wall == self.map.get_tile(self.start_pos.x, self.start_pos.y) {
            self.start_pos.x -= 1;
        }

        let starts = vec![self.map.index(self.start_pos.x, self.start_pos.y)];
        self.map.populate_blocked();
        let dijkstra = DijkstraMap::new(
            self.map.width(),
            self.map.height(),
            &starts,
            &self.map,
            200.,
        );
        let mut exit_tile = (0usize, 0f32);
        for (i, dist) in dijkstra.map.iter().enumerate() {
            if *dist == std::f32::MAX {
                self.map.set_tile_by_index(i, TileType::Wall);
            } else if *dist > exit_tile.1 {
                exit_tile = (i, *dist);
            }
        }
        self.map
            .set_tile_by_index(exit_tile.0, TileType::DownStairs);
        self.take_snapshot();
    }

    fn take_snapshot(&mut self) {
        if SHOW_MAPGEN_VISUALIZER {
            let mut map = self.map.clone();
            map.reveal_everything();
            self.history.push(map);
        }
    }
}

mod tests {
    #[test]
    fn consistency_check() {
        use super::CellularAutomataBuilder;
        use crate::map_builders::MapBuilder;

        let mut builder = CellularAutomataBuilder::new(42, 1);
        builder.build_map();
        let map1 = builder.get_map();

        let mut builder = CellularAutomataBuilder::new(42, 1);
        builder.build_map();
        let map2 = builder.get_map();
        for y in 0..map1.height() {
            for x in 0..map1.width() {
                assert_eq!(
                    map1.get_tile(x, y),
                    map2.get_tile(x, y),
                    "checking position ({},{})",
                    x,
                    y
                );
            }
        }
    }
}
