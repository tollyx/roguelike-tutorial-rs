use super::{common, MapBuilder};
use crate::{
    component::Position,
    map::{Map, TileType},
    rect::Rect,
    spawner, SHOW_MAPGEN_VISUALIZER, WINDOW_HEIGHT, WINDOW_WIDTH,
};
use bracket_random::prelude::RandomNumberGenerator;

const MAX_ROOM_SIZE: i32 = 14;
const MIN_ROOM_SIZE: i32 = 4;

pub struct BspInteriorBuilder {
    rng: RandomNumberGenerator,
    map: Map,
    start_pos: Position,
    depth: i32,
    rooms: Vec<Rect>,
    history: Vec<Map>,
    rects: Vec<Rect>,
}

impl BspInteriorBuilder {
    pub fn new(seed: u64, depth: i32) -> BspInteriorBuilder {
        let mut map = Map::new(WINDOW_WIDTH, WINDOW_HEIGHT - 9, TileType::Wall);
        map.depth = depth;
        BspInteriorBuilder {
            depth,
            history: vec![],
            map,
            rects: vec![],
            rng: RandomNumberGenerator::seeded(seed),
            rooms: vec![],
            start_pos: Position { x: 0, y: 0 },
        }
    }

    fn take_snapshot(&mut self) {
        if SHOW_MAPGEN_VISUALIZER {
            let mut map = self.map.clone();
            map.reveal_everything();
            self.history.push(map);
        }
    }

    fn build(&mut self) {
        self.rects.clear();
        self.rects
            .push(Rect::new(1, 1, self.map.width() - 2, self.map.height() - 2));
        self.add_subrects(&self.rects.first().unwrap().clone());

        let rooms = self.rects.clone();
        for r in &rooms {
            self.rooms.push(*r);
            common::apply_room(&mut self.map, r);
            self.take_snapshot();
        }

        match self.rng.range(0, 4) {
            0 => self.rooms.sort_by(|a, b| a.left.cmp(&b.left)),
            1 => self.rooms.sort_by(|a, b| a.top.cmp(&b.top)),
            2 => self.rooms.sort_by(|a, b| a.right.cmp(&b.right).reverse()),
            3 => self.rooms.sort_by(|a, b| a.bottom.cmp(&b.bottom).reverse()),
            _ => unreachable!(),
        }

        for i in 0..self.rooms.len() - 1 {
            let room = &self.rooms[i];
            let room_next = &self.rooms[i + 1];
            let sx = self.rng.range(room.left, room.right);
            let sy = self.rng.range(room.top, room.bottom);

            let ex = self.rng.range(room_next.left, room_next.right);
            let ey = self.rng.range(room_next.top, room_next.bottom);

            self.draw_corridor(sx, sy, ex, ey);
            self.take_snapshot();
        }

        self.start_pos = self.rooms.first().unwrap().center().into();
    }

    fn add_subrects(&mut self, rect: &Rect) {
        let (width, height) = rect.dimensions();
        let (half_w, half_h) = (width / 2, height / 2);

        if half_w <= MIN_ROOM_SIZE + 1 && half_h <= MIN_ROOM_SIZE + 1 {
            // room is too small to split
            return;
        }

        if width <= MAX_ROOM_SIZE && height <= MAX_ROOM_SIZE {
            // room is ok size, roll if we should keep it
            if self.rng.range(0, 2) == 0 {
                return;
            }
        }

        if !self.rects.is_empty() {
            self.rects.remove(self.rects.len() - 1);
        }

        enum Split {
            Hori,
            Vert,
            None,
        }

        let split = match (half_w > MIN_ROOM_SIZE, half_h > MIN_ROOM_SIZE) {
            (true, true) => {
                // roll for horis/vert
                if self.rng.range(0, 2) == 0 {
                    Split::Hori
                } else {
                    Split::Vert
                }
            }
            (false, true) => Split::Hori,
            (true, false) => Split::Vert,
            (false, false) => Split::None,
        };

        match split {
            Split::Hori => {
                let new_height = if half_h == MIN_ROOM_SIZE {
                    MIN_ROOM_SIZE
                } else {
                    self.rng.range(MIN_ROOM_SIZE + 1, height - MIN_ROOM_SIZE)
                };

                let h1 = Rect::new(rect.left, rect.top, width, new_height - 1);
                let h2 = Rect::new(rect.left, rect.top + new_height, width, height - new_height);

                self.rects.push(h1);
                self.add_subrects(&h1);
                self.rects.push(h2);
                self.add_subrects(&h2);
            }
            Split::Vert => {
                let new_width = if half_w == MIN_ROOM_SIZE {
                    MIN_ROOM_SIZE
                } else {
                    self.rng.range(MIN_ROOM_SIZE + 1, width - MIN_ROOM_SIZE)
                };

                let v1 = Rect::new(rect.left, rect.top, new_width - 1, height);
                let v2 = Rect::new(rect.left + new_width, rect.top, width - new_width, height);

                self.rects.push(v1);
                self.add_subrects(&v1);
                self.rects.push(v2);
                self.add_subrects(&v2);
            }
            Split::None => {
                print!("If this is printed, then it means we got a bug in interior bsp mapgen :)");
                self.rects.push(*rect);
            }
        }
    }

    fn draw_corridor(&mut self, sx: i32, sy: i32, ex: i32, ey: i32) {
        // if self.rng.range(0, 2) == 0 {
        //     apply_tunnel_x(&mut self.map, sx, ex, ey);
        //     apply_tunnel_y(&mut self.map, sy, ey, ex);
        // } else {
        //     apply_tunnel_y(&mut self.map, sy, ey, ex);
        //     apply_tunnel_x(&mut self.map, sx, ex, ey);
        // }
        common::apply_tunnel_xy_rng(&mut self.rng, &mut self.map, sx, sy, ex, ey)
    }
}

impl MapBuilder for BspInteriorBuilder {
    fn build_map(&mut self) {
        self.build()
    }
    fn spawn_entities(&mut self, ecs: &mut specs::World) {
        for room in self.rooms.iter().skip(1) {
            spawner::populate_room(&mut self.rng, ecs, room, self.depth);
        }
    }
    fn get_map(&mut self) -> Map {
        self.map.clone()
    }
    fn get_starting_position(&mut self) -> Position {
        self.start_pos.clone()
    }
    fn get_snapshot_history(&self) -> &Vec<Map> {
        &self.history
    }
}

mod tests {
    #[test]
    fn consistency_check() {
        use super::BspInteriorBuilder;
        use crate::map_builders::MapBuilder;

        let mut builder = BspInteriorBuilder::new(42, 1);
        builder.build_map();
        let map1 = builder.get_map();

        let mut builder = BspInteriorBuilder::new(42, 1);
        builder.build_map();
        let map2 = builder.get_map();
        for y in 0..map1.height() {
            for x in 0..map1.width() {
                assert_eq!(
                    map1.get_tile(x, y),
                    map2.get_tile(x, y),
                    "checking position ({},{})",
                    x,
                    y
                );
            }
        }
    }
}
