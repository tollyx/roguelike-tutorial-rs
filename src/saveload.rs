use crate::{
    component::*,
    gamelog::{clear_log, Logger},
    map::Map,
};
use bracket_random::prelude::RandomNumberGenerator;
use flate2::{read::ZlibDecoder, write::ZlibEncoder};
use ron::{ser::PrettyConfig, Options};
use std::convert::Infallible;
use specs::prelude::*;
use specs::saveload::*;
use std::fs::File;
use std::io::{Read, Write};

const SAVEFILE_NAME: &str = "savegame";

macro_rules! serialize_individually {
    ($ecs:expr, $ser:expr, $data:expr, $( $type:ty),*) => {
        $(
        SerializeComponents::<Infallible, SimpleMarker<SerializeMe>>::serialize(
            &( $ecs.read_storage::<$type>(), ),
            &$data.0,
            &$data.1,
            &mut $ser,
        )
        .unwrap();
        )*
    };
}

macro_rules! deserialize_individually {
    ($ecs:expr, $de:expr, $data:expr, $( $type:ty),*) => {
        $(
        DeserializeComponents::<Infallible, _>::deserialize(
            &mut ( &mut $ecs.write_storage::<$type>(), ),
            & $data.0, // entities
            &mut $data.1, // marker
            &mut $data.2, // allocater
            &mut $de,
        )
        .unwrap();
        )*
    };
}

fn project_dirs() -> directories::ProjectDirs {
    directories::ProjectDirs::from("net", "tollyx", "spork").unwrap()
}

pub fn save_game_to_file(ecs: &mut World) {
    let bytes = save_game_to_bytes(ecs);

    let projdirs = project_dirs();
    let datadir = projdirs.data_dir();
    let savefile = datadir.join(SAVEFILE_NAME);

    if !datadir.exists() {
        std::fs::create_dir_all(&datadir).expect("Failed creating the game data dir");
    } else if savefile.exists() {
        std::fs::remove_file(&savefile).expect("Failed deleting old save file");
    }

    let mut file = File::create(&savefile).expect("Failed creating save file");
    file.write_all(&bytes).expect("Failed writing save file");
}

pub fn save_game_to_bytes(ecs: &mut World) -> Vec<u8> {
    let mapcopy = ecs.get_mut::<Map>().unwrap().clone();
    let savehelper = ecs
        .create_entity()
        .with(SerializationHelper { map: mapcopy })
        .marked::<SimpleMarker<SerializeMe>>()
        .build();

    let bytes = {
        let data = (
            ecs.entities(),
            ecs.read_storage::<SimpleMarker<SerializeMe>>(),
        );

        let mut compressor = ZlibEncoder::new(vec![], flate2::Compression::default());
        let mut serializer = ron::ser::Serializer::with_options(&mut compressor, Some(PrettyConfig::default().struct_names(true)), Options::default()).unwrap();
        serialize_individually!(
            ecs,
            serializer,
            data,
            AreaOfEffect,
            BlocksTile,
            CombatStats,
            Confusion,
            Consumable,
            EntityMoved,
            EntryTrigger,
            Equippable,
            Equipped,
            Hidden,
            HungerClock,
            InContainer,
            InflictsDamage,
            Invulnerable,
            Item,
            MagicMapper,
            Monster,
            Name,
            Player,
            Position,
            ProvidesFood,
            ProvidesHealing,
            Ranged,
            Renderable,
            SerializationHelper,
            SingleActivation,
            SufferDamage,
            Viewshed,
            WantsToDropItem,
            WantsToMelee,
            WantsToPickupItem,
            WantsToUseItem
        );

        compressor.finish().expect("Failed compressing save data")
    };
    ecs.delete_entity(savehelper)
        .expect("Couldn't cleanup after save");

    bytes
}

pub fn load_game_from_file(ecs: &mut World) {
    let bytes = std::fs::read(project_dirs().data_dir().join(SAVEFILE_NAME))
        .expect("Failed reading save file");
    load_game_from_bytes(ecs, bytes)
}

pub fn load_game_from_bytes(ecs: &mut World, bytes: Vec<u8>) {
    ecs.delete_all();

    let mut decoder = ZlibDecoder::new(&bytes[..]);
    let mut data_string = String::new();
    decoder
        .read_to_string(&mut data_string)
        .expect("Failed decompressing save file");

    let mut de = ron::de::Deserializer::from_str(&data_string).expect("Failed parsing save file");
    {
        let mut d = (
            &mut ecs.entities(),
            &mut ecs.write_storage::<SimpleMarker<SerializeMe>>(),
            &mut ecs.write_resource::<SimpleMarkerAllocator<SerializeMe>>(),
        );

        deserialize_individually!(
            ecs,
            de,
            d,
            AreaOfEffect,
            BlocksTile,
            CombatStats,
            Confusion,
            Consumable,
            EntityMoved,
            EntryTrigger,
            Equippable,
            Equipped,
            Hidden,
            HungerClock,
            InContainer,
            InflictsDamage,
            Invulnerable,
            Item,
            MagicMapper,
            Monster,
            Name,
            Player,
            Position,
            ProvidesFood,
            ProvidesHealing,
            Ranged,
            Renderable,
            SerializationHelper,
            SingleActivation,
            SufferDamage,
            Viewshed,
            WantsToDropItem,
            WantsToMelee,
            WantsToPickupItem,
            WantsToUseItem
        );
    }

    // Scope to get around the ecs's borrow checks
    let (map, player_pos, player_ent, helper_ent) = {
        let entities = ecs.entities();
        let helper = ecs.read_storage::<SerializationHelper>();
        let player = ecs.read_storage::<Player>();
        let position = ecs.read_storage::<Position>();

        let (helper_ent, helper) = (&entities, &helper)
            .join()
            .next()
            .expect("No helper entity found");
        let (player_ent, _p, player_pos) = (&entities, &player, &position)
            .join()
            .next()
            .expect("No player entity found");
        let mut map = helper.map.clone();
        map.reinit_content_index();
        (
            map,
            PlayerPos((player_pos.x, player_pos.y).into()),
            PlayerEnt(player_ent),
            helper_ent,
        )
    };

    ecs.insert(map);
    ecs.insert(player_pos);
    ecs.insert(player_ent);
    ecs.insert(RandomNumberGenerator::new()); // TODO: carry over previous rng from save
    ecs.delete_entity(helper_ent)
        .expect("Unable to delete helper");
    clear_log();
    Logger::new().append("Game loaded!").log();
}

pub fn delete_save() {
    let projdirs = project_dirs();
    let path = projdirs.data_dir().join(SAVEFILE_NAME);
    if path.exists() {
        std::fs::remove_file(path).expect("Couldn't delete savefile");
    }
}

pub fn save_exists() -> bool {
    project_dirs().data_dir().join(SAVEFILE_NAME).exists()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn empty_saveload() {
        let mut ecs = World::new();
        crate::component::register_all(&mut ecs);
        ecs.insert(crate::map::Map::new(3, 3, crate::map::TileType::Wall));
        let ent = crate::spawner::player(&mut ecs, 1, 1);
        ecs.insert(crate::component::PlayerEnt(ent));
        ecs.insert(crate::component::PlayerPos((1, 1).into()));

        let bytes = save_game_to_bytes(&mut ecs);
        load_game_from_bytes(&mut ecs, bytes);
    }
}
